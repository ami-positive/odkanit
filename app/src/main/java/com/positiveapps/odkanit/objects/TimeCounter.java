package com.positiveapps.odkanit.objects;

import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.positiveapps.odkanit.main.MyApp;

/**
 * Created by Izakos on 27/12/2015.
 */
public class TimeCounter {

    private final String TAG = "TimeCounter";

    public static final int STATE_STOPED = 0;
    public static final int STATE_RUNNING = 1;
    public static final int STATE_PAUSED = 2;


    private ImageButton stopBT;
    private ImageButton startBT;
    private ImageButton pauseBT;
    private ImageButton resumeBT;
    private ImageButton manualBT;


    private static TimeCounter instance;

    private int timeMode = MyApp.userSettings.getTimerState();
    private long startCount = MyApp.userSettings.getTimerStart();
    private long difference = MyApp.userSettings.getTimerDifference();
    private long timeWhenStopped = MyApp.userSettings.getTimeWhenStopped();

    private TimeCounter () {

    }

    public static TimeCounter getInstance () {
        if (instance == null){
            instance = new TimeCounter();
        }


        return instance;
    }

    public static void removeInstance () {
        instance = null;
    }

    public void setButtons(ImageButton stopBT, ImageButton startBT, ImageButton pauseBT, ImageButton resumeBT, ImageButton manualBT){
        this.stopBT = stopBT;
        this.startBT = startBT;
        this.pauseBT = pauseBT;
        this.resumeBT = resumeBT;
        this.manualBT = manualBT;
        rotateView(timeMode);

    }


    public void start() {
        switch (timeMode) {
            case STATE_RUNNING:
                Log.i(TAG, "start -> STATE_RUNNING");
                break;
            case STATE_STOPED:
                Log.i(TAG, "start -> STATE_STOPED");
                difference = 0;
                startCount = System.currentTimeMillis();
                timeMode = STATE_RUNNING;
                break;
            case STATE_PAUSED:
                Log.i(TAG, "start -> STATE_PAUSED");
                break;
        }
        rotateView(timeMode);
    }

    public void pause(){
        switch (timeMode) {
            case STATE_RUNNING:
                Log.i(TAG, "pause -> STATE_RUNNING");
                difference += System.currentTimeMillis() - startCount;
                timeWhenStopped = System.currentTimeMillis();
                timeMode = STATE_PAUSED;
                break;
            case STATE_STOPED:
                Log.i(TAG, "pause -> STATE_STOPED");
                break;
            case STATE_PAUSED:
                Log.i(TAG, "pause -> STATE_PAUSED");
                break;
        }
        rotateView(timeMode);
    }

    public void stop(){
        switch (timeMode) {
            case STATE_RUNNING:
                Log.i(TAG, "stop -> STATE_RUNNING");
                difference = System.currentTimeMillis() - startCount;
                timeMode = STATE_STOPED;
                break;
            case STATE_STOPED:
                Log.i(TAG, "stop -> STATE_STOPED");
                break;
            case STATE_PAUSED:
                Log.i(TAG, "stop -> STATE_PAUSED");
                difference += System.currentTimeMillis() - startCount;
                timeMode = STATE_STOPED;
                break;
        }
        rotateView(timeMode);
    }

    public void resume(){
        switch (timeMode) {
            case STATE_RUNNING:
                Log.i(TAG, "resume -> STATE_RUNNING");
                break;
            case STATE_STOPED:
                Log.i(TAG, "resume -> STATE_STOPED");
                break;
            case STATE_PAUSED:
                Log.i(TAG, "resume -> STATE_PAUSED");
                startCount = System.currentTimeMillis();
                timeMode = STATE_RUNNING;
                break;
        }
        rotateView(timeMode);
    }



    private void rotateView(int mode) {
        System.out.println("mode" + mode);
        View[] views = {manualBT, pauseBT, resumeBT};
        for (int i = 0; i < views.length; i++) {
            if (i != mode) {
                System.out.println("mode1 no" + mode);
                views[i].setRotationY(90f);
                views[i].setAlpha(0);
            } else {
                System.out.println("mode1 yes" + mode);
                views[i].setRotationY(0f);
                views[i].setAlpha(1);
            }
        }

        View[] views2 = {startBT, stopBT};
        if(mode == 2) mode = 1; // same button
        for (int i = 0; i < views2.length; i++) {
            if (i != mode) {
                System.out.println("mode2 no" + mode);
                views2[i].setRotationY(90f);
                views2[i].setAlpha(0);
            } else {
                System.out.println("mode2 yes" + mode);
                views2[i].setRotationY(0f);
                views2[i].setAlpha(1);
            }
        }

    }

    public void initTimerUI(){
        rotateView(timeMode);
    }

    public int getState(){
        return timeMode;
    }

    public long getDifference() {
        return difference;
    }

    public long getStartCount() {
        return startCount;
    }

    public long getTimeWhenStopped() {
        return timeWhenStopped;
    }

    public void setTimeWhenStopped(long timeWhenStopped) {
        this.timeWhenStopped = timeWhenStopped;
    }
}
