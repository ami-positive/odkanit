package com.positiveapps.odkanit.objects;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.util.AppUtil;
import com.positiveapps.positiveapps.odcanit.R;

/**
 * Created by NIVO on 21/12/2015.
 */
public class TextBox extends LinearLayout{

    private boolean isLogin;
    private boolean isDescription;
    private LinearLayout textBoxLinear;
    private boolean isMultiLines;
    private boolean istextHint;
    private LinearLayout timerButtonLayout;
    private boolean isEditText;
    private boolean isTimer;
    private TextView iconText;
    private EditText mainText;
    private ImageView iconImg;
    private int iconRes;
    private String iconName;
    private String text;

    private ImageButton start;
    private ImageButton stop;
    private ImageButton manual;
    private ImageButton resume;
    private ImageButton pause;
    private Chronometer chronometer;
    private FrameLayout frameRightSide;
    private FrameLayout textBoxBasicFrame;
    public static boolean isEditTextOpen;

    public TextBox(Context context) {
        super(context);
    }

    public TextBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextBox,0,0 );

        iconRes = array.getResourceId(R.styleable.TextBox_Icon, R.mipmap.customer_code_login_icon);
        iconName = array.getString(R.styleable.TextBox_IconName);
        text = array.getString(R.styleable.TextBox_Text);
        isEditText = array.getBoolean(R.styleable.TextBox_isEditText, true);
        isTimer = array.getBoolean(R.styleable.TextBox_isTimer, false);
        istextHint = array.getBoolean(R.styleable.TextBox_istextHint, false);
        isMultiLines = array.getBoolean(R.styleable.TextBox_isMultiLines, false);
        isDescription = array.getBoolean(R.styleable.TextBox_isDescription, false);
        isLogin = array.getBoolean(R.styleable.TextBox_isLogin, false);


        LayoutInflater.from(context).inflate(R.layout.text_box_item, this);


        iconText = (TextView) findViewById(R.id.iconText);
        mainText = (EditText) findViewById(R.id.mainText);
        iconImg = (ImageView) findViewById(R.id.iconImage);

        timerButtonLayout= (LinearLayout)findViewById(R.id.timerButtonLayout);
        frameRightSide=(FrameLayout)findViewById(R.id.frameRightSide);

        start = (ImageButton)findViewById(R.id.start);
        stop= (ImageButton)findViewById(R.id.stop);
        manual = (ImageButton)findViewById(R.id.manual);
        resume= (ImageButton)findViewById(R.id.resume);
        pause= (ImageButton)findViewById(R.id.pause);
        chronometer = (Chronometer)findViewById(R.id.chronometer);
        textBoxLinear = (LinearLayout)findViewById(R.id.textBoxLinear);
        textBoxBasicFrame= (FrameLayout)findViewById(R.id.textBoxBasicFrame);


//        mainText.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                textBoxLinear.animate().translationX(frameRightSide.getWidth());
//                lastFrameRightSide = frameRightSide;
//            }
//        });



        if (isLogin){
            iconText.setVisibility(GONE);
            int size = (int) AppUtil.convertDpToPixel(30, MyApp.appContext);
            int size2 = (int) AppUtil.convertDpToPixel(12, MyApp.appContext);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(size,size);
            lp.setMargins(size2, 0, 0, 0);
            iconImg.setLayoutParams(lp);
        }

        if (isDescription) {
            mainText.setOnFocusChangeListener(new OnFocusChangeListener() {

                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus &&  isEditTextOpen) {
                        textBoxLinear.animate().translationX(0).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                textBoxLinear.getLayoutParams().width = textBoxLinear.getWidth() - frameRightSide.getWidth();
                                textBoxLinear.requestLayout();
                                isEditTextOpen = false;
                            }
                        });
                    } else if(hasFocus &&  !isEditTextOpen) {
                        textBoxLinear.animate().translationX(frameRightSide.getWidth()).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                textBoxLinear.getLayoutParams().width = frameRightSide.getWidth() + textBoxLinear.getWidth();
                                textBoxLinear.requestLayout();
                                textBoxLinear.setTranslationX(0);
                                isEditTextOpen = true;
                            }
                        });
                    }
                }
            });

        }

        if (istextHint){
            mainText.setHint(text);
        }else {
            mainText.setText(text);

        }

        if(isEditText){
            mainText.setFocusableInTouchMode(true);
        }else{
            mainText.setFocusable(false);
        }

        if (isTimer){
            timerButtonLayout.setVisibility(VISIBLE);


        }else {
            timerButtonLayout.setVisibility(GONE);

        }

        if(isMultiLines){
            mainText.setMaxLines(4);
            ViewGroup.LayoutParams params = textBoxLinear.getLayoutParams();
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            textBoxLinear.requestLayout();
        }else{
            mainText.setSingleLine();
        }

        mainText.setEllipsize(TextUtils.TruncateAt.END);



        iconText.setText(iconName);
        iconImg.setImageResource(iconRes);

    }


    public EditText getEditText(){

        return mainText;
    }



    public void removeError(){
        if (!mainText.getText().toString().equals("")){
            mainText.setError(null);
        }
    }

    public FrameLayout getBasicFrame(){
        return textBoxBasicFrame;
    }

    public FrameLayout getFrameRightSide(){
        return frameRightSide;
    }


    public ImageButton getStart() {
        return start;
    }

    public ImageButton getStop() {
        return stop;
    }

    public ImageButton getManual() {
        return manual;
    }

    public ImageButton getResume() {
        return resume;
    }

    public ImageButton getPause() {
        return pause;
    }

    public void setMainText(String s){
        mainText.setError(null);
        mainText.setText(s);
    }
    public String getMainText(){

        return mainText.getText().toString();
    }

    public EditText getMainEditText(){
        return mainText;
    }




    public Chronometer getChronometer() {
        return chronometer;
    }


    public void setOnViewClickListener (OnClickListener onViewClickListener) {
        setOnClickListener(onViewClickListener);
    }

    public void removeFocus(){
        findViewById(R.id.dummyEditext).requestFocus();
    }

}
