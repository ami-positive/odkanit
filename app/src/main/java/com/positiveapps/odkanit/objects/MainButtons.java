package com.positiveapps.odkanit.objects;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.positiveapps.odcanit.R;

/**
 * Created by NIVO on 23/12/2015.
 */
public class MainButtons extends LinearLayout{
    private Button sefer_telefonim_button;
    private Button nohahut_button;
    private Button hozaot_button;
    private Button buttonDoh_mesakem;
    private TextView iconText;
    private TextView doh_mesakem;
    private TextView sefer_telefonim;
    private TextView divuah_mehirot;
    private TextView divuah_hozaot;
    private String button2Name;
    private String button3Name;
    private String button4Name;
    private String button1Name;

    public MainButtons(Context context) {
        super(context);
    }

    public MainButtons(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MainButtons,0,0 );

        LayoutInflater.from(context).inflate(R.layout.main_buttons, this);

        button1Name = array.getString(R.styleable.MainButtons_button1Name);
        button2Name = array.getString(R.styleable.MainButtons_button2Name);
        button3Name = array.getString(R.styleable.MainButtons_button3Name);
        button4Name = array.getString(R.styleable.MainButtons_button4Name);

        doh_mesakem     = (TextView) findViewById(R.id.doh_mesakem);
        sefer_telefonim = (TextView) findViewById(R.id.sefer_telefonim);
        divuah_mehirot  = (TextView) findViewById(R.id.divuah_mehirot);
        divuah_hozaot   = (TextView) findViewById(R.id.divuah_hozaot);

        buttonDoh_mesakem= (Button)findViewById(R.id.buttonDoh_mesakem);
        sefer_telefonim_button= (Button)findViewById(R.id.sefer_telefonim_button);
        nohahut_button= (Button)findViewById(R.id.nohahut_button);
        hozaot_button= (Button)findViewById(R.id.hozaot_button);

        doh_mesakem    .setText(button1Name);
        sefer_telefonim.setText(button2Name);
        divuah_mehirot .setText(button3Name);
       divuah_hozaot  .setText(button4Name);





    }

    public Button buttonDoh_mesakem(){
        return buttonDoh_mesakem;
    }

    public Button sefer_telefonim_button(){
        return sefer_telefonim_button;
    }
    public Button nohahut_button(){
        return nohahut_button;
    }
    public Button hozaot_button(){
        return hozaot_button;
    }
}


