package com.positiveapps.odkanit.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.DatabaseHelper;
import com.positiveapps.odkanit.database.tables.TableLastClients;

/**
 * Created by NIVO on 30/12/2015.
 */
public class LastClientsContentProvider extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "com.positiveapps.odkanit.database.providers.LastClientsContentProvider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "lastsclient";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return TableLastClients.TABLE_CLIENT;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TableLastClients.TAG_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TableLastClients.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}
