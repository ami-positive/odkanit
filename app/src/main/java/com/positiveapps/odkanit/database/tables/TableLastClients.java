package com.positiveapps.odkanit.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.ContentProvaiderDataSourseAdapter;
import com.positiveapps.odkanit.database.providers.LastClientsContentProvider;
import com.positiveapps.odkanit.models.ClientOBJ;

/**
 * Created by NIVO on 13/12/2015.
 */
public class TableLastClients extends ContentProvaiderDataSourseAdapter<ClientOBJ> {

    // table name
    public static final String TABLE_CLIENT = "lastsclient";

    // columns names
    public final static String TAG_ID           = "_id";
    public final static String TAG_NAME         = "name";
    public final static String TAG_NUMBER         = "number";
    public final static String TAG_TIME   = "miltime";


    public static final String[] ALL_COLUMNS = {
         TAG_ID,TAG_NAME,TAG_NUMBER,TAG_TIME

    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_CLIENT
            + "("
            + TAG_ID               + " integer primary key autoincrement, "
            + TAG_NAME            + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_NUMBER            + " text not null DEFAULT " + DEFAULT_TEXT_VALUE+ ", "
            + TAG_TIME              + " integer"
            +");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT);
        onCreate(database);
    }


    public static TableLastClients instance;



    public static TableLastClients getInstance (Context context){
        if (instance == null){
            instance = new TableLastClients(context);
        }
        return instance;
    }


    private TableLastClients(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return LastClientsContentProvider.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null){
            databasehelper.close();
        }

        instance = null;
    }



    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }


    @Override
    public ClientOBJ cursorToEntity(Cursor cursor) {



        long id=  cursor.getLong(cursor.getColumnIndexOrThrow(TableLastClients.TAG_ID));
        String name=cursor.getString(cursor.getColumnIndexOrThrow(TableLastClients.TAG_NAME));
        String number=cursor.getString(cursor.getColumnIndexOrThrow(TableLastClients.TAG_NUMBER));
        long time= cursor.getLong(cursor.getColumnIndexOrThrow(Table10Contacts.TAG_TIME));

        ClientOBJ clientOBJ = new ClientOBJ(id,name,number,time);


        return clientOBJ;
    }

    @Override
    public ContentValues entityToContentValue(ClientOBJ entity) {
        ContentValues values = new ContentValues();

        values.put(TableLastClients.TAG_NAME, entity.getName());
        values.put(TableLastClients.TAG_NUMBER, entity.getNumber());
        values.put(Table10Contacts.TAG_TIME, entity.getTimeInMil());
        return values;
    }



    @Override
    protected BaseContentProvider createContentProvider() {
        return new LastClientsContentProvider();
    }


    @Override
    public synchronized ClientOBJ insert(ClientOBJ entity) {
        return super.insert(entity);

    }

    public ClientOBJ isEntryAllradyExsit (ClientOBJ toCheck){

        try {
            ClientOBJ msg = readEntityByID(toCheck.getId());
            if (msg != null){
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public ClientOBJ insertOrUpdate (ClientOBJ toAdd) {
        ClientOBJ check = isEntryAllradyExsit(toAdd);
        if (check != null){
            return update(check.getId(), toAdd);
        }else{
            return insert(toAdd);
        }
    }
}

