package com.positiveapps.odkanit.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.ContentProvaiderDataSourseAdapter;
import com.positiveapps.odkanit.database.providers.CustomerContentProvider;
import com.positiveapps.odkanit.database.providers.TikContentProvider;
import com.positiveapps.odkanit.models.ClientOBJ;
import com.positiveapps.odkanit.models.Tik;

/**
 * Created by NIVO on 13/12/2015.
 */
public class TableTik extends ContentProvaiderDataSourseAdapter<Tik> {

    // table name
    public static final String TABLE_TIK = "tiks";

    // columns names
    public final static String TAG_ID           = "_id";
    public final static String TAG_NAME         = "name";
    public final static String TAG_NUMBER         = "number";


    public static final String[] ALL_COLUMNS = {
         TAG_ID,TAG_NAME,TAG_NUMBER

    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_TIK
            + "("
            + TAG_ID               + " integer primary key autoincrement, "
            + TAG_NAME            + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_NUMBER            + " text not null DEFAULT " + DEFAULT_TEXT_VALUE
            +");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_TIK);
        onCreate(database);
    }


    public static TableTik instance;



    public static TableTik getInstance (Context context){
        if (instance == null){
            instance = new TableTik(context);
        }
        return instance;
    }


    private TableTik(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return TikContentProvider.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null){
            databasehelper.close();
        }

        instance = null;
    }



    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }


    @Override
    public Tik cursorToEntity(Cursor cursor) {



        long id=  cursor.getLong(cursor.getColumnIndexOrThrow(TableTik.TAG_ID));
        String name=cursor.getString(cursor.getColumnIndexOrThrow(TableTik.TAG_NAME));
        String number=cursor.getString(cursor.getColumnIndexOrThrow(TableTik.TAG_NUMBER));

        Tik tik = new Tik(id,name,number);


        return tik;
    }

    @Override
    public ContentValues entityToContentValue(Tik entity) {
        ContentValues values = new ContentValues();

        values.put(TableTik.TAG_NAME, entity.getName());
        values.put(TableTik.TAG_NUMBER, entity.getNumber());
        return values;
    }



    @Override
    protected BaseContentProvider createContentProvider() {
        return new TikContentProvider();
    }


    @Override
    public synchronized Tik insert(Tik entity) {
        return super.insert(entity);

    }

    public Tik isEntryAllradyExsit (Tik toCheck){

        try {
            Tik msg = readEntityByID(toCheck.getId());
            if (msg != null){
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public Tik insertOrUpdate (Tik toAdd) {
        Tik check = isEntryAllradyExsit(toAdd);
        if (check != null){
            return update(check.getId(), toAdd);
        }else{
            return insert(toAdd);
        }
    }
}

