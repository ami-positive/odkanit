package com.positiveapps.odkanit.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.DatabaseHelper;
import com.positiveapps.odkanit.database.tables.TableBilling;
import com.positiveapps.odkanit.database.tables.TableClient;

/**
 * Created by NIVO on 30/12/2015.
 */
public class ClientContentProvider extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "com.positiveapps.odkanit.database.providers.ClientContentProvider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "client";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return TableClient.TABLE_CLIENT;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TableClient.TAG_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TableClient.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}
