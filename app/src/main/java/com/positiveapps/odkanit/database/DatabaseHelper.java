/**
 * 
 */
package com.positiveapps.odkanit.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.odkanit.database.tables.Table10Contacts;
import com.positiveapps.odkanit.database.tables.TableBilling;
import com.positiveapps.odkanit.database.tables.TableClient;
import com.positiveapps.odkanit.database.tables.TableCustomer;
import com.positiveapps.odkanit.database.tables.TableLastClients;
import com.positiveapps.odkanit.database.tables.TableTik;

/**
 * @author natiapplications
 * 
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	// database name
	private static final String DATABASE_NAME = "odkanit.db";
	// database version
	private static final int DATABASE_VERSION = 10;

	

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		TableCustomer.onCreate(database);
		TableBilling.onCreate(database);
		TableClient.onCreate(database);
		TableTik.onCreate(database);
		Table10Contacts.onCreate(database);
		TableLastClients.onCreate(database);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		TableCustomer.onUpgrade(database, oldVersion, newVersion);
		TableBilling.onUpgrade(database, oldVersion, newVersion);
		TableClient.onUpgrade(database,oldVersion,newVersion);
		TableTik.onUpgrade(database,oldVersion,newVersion);
		Table10Contacts.onUpgrade(database,oldVersion,newVersion);
		TableLastClients.onUpgrade(database,oldVersion,newVersion);

	}

}
