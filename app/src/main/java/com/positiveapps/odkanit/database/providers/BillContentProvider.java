package com.positiveapps.odkanit.database.providers;

import android.database.sqlite.SQLiteOpenHelper;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.DatabaseHelper;
import com.positiveapps.odkanit.database.tables.TableBilling;


public class BillContentProvider extends BaseContentProvider {

    @Override
    protected String provideAuthority() {
        // TODO Auto-generated method stub
        return "com.positiveapps.odkanit.database.providers.BillContentProvider";
    }


    @Override
    protected String provideBasePath() {
        // TODO Auto-generated method stub
        return "bill";
    }


    @Override
    protected String provideTableName() {
        // TODO Auto-generated method stub
        return TableBilling.TABLE_BILL;
    }


    @Override
    protected String provideColumnID() {
        // TODO Auto-generated method stub
        return TableBilling.TAG_ID;
    }


    @Override
    protected String[] provideAllColumns() {
        // TODO Auto-generated method stub
        return TableBilling.ALL_COLUMNS;
    }


    @Override
    protected SQLiteOpenHelper provideSQLiteHalper() {
        // TODO Auto-generated method stub
        return new DatabaseHelper(getContext());
    }


}

