package com.positiveapps.odkanit.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.positiveapps.odkanit.database.BaseContentProvider;
import com.positiveapps.odkanit.database.ContentProvaiderDataSourseAdapter;
import com.positiveapps.odkanit.models.Bill;
import com.positiveapps.odkanit.database.providers.BillContentProvider;
public class TableBilling extends ContentProvaiderDataSourseAdapter<Bill> {

    // table name
    public static final String TABLE_BILL= "bill";

    // columns names
    public final static String TAG_ID           = "_id";
    public final static String TAG_DATE         = "date";
    public final static String TAG_CUSTOMER_NAME = "customer";
    public final static String TAG_BAG          = "bag";
    public final static String TAG_time         = "time";
    public final static String TAG_DESCRIPTION  = "description";
    public final static String TAG_LAT  = "Latitude";
    public final static String TAG_LONG  = "Longitude";

    
    public static final String[] ALL_COLUMNS = {
         TAG_ID,TAG_DATE,TAG_CUSTOMER_NAME,TAG_BAG,TAG_time,TAG_DESCRIPTION,TAG_LAT,TAG_LONG

    };

    public static final int COLUMN_SIZE = ALL_COLUMNS.length;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table " + TABLE_BILL
            + "("
            + TAG_ID                + " integer primary key autoincrement, "
            + TAG_DATE             + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_CUSTOMER_NAME   + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_BAG                + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_time                     + " text not null DEFAULT " + DEFAULT_TEXT_VALUE + ", "
            + TAG_DESCRIPTION            + " text not null DEFAULT " + DEFAULT_TEXT_VALUE+ ", "
            + TAG_LAT                   + " REAL , "
            + TAG_LONG                   + " REAL"
            +");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_BILL);
        onCreate(database);
    }


    public static TableBilling instance;



    public static TableBilling getInstance (Context context){
        if (instance == null){
            instance = new TableBilling(context);
        }
        return instance;
    }


    private TableBilling(Context context) {
        super(context);

    }


    @Override
    protected Uri getProviderUri() {
        return BillContentProvider.CONTENT_URI;
    }

    @Override
    public void close() {
        if (databasehelper != null){
            databasehelper.close();
        }

        instance = null;
    }



    @Override
    protected void onCreateDataSourse(BaseContentProvider contentProvider) {
        super.onCreateDataSourse(contentProvider);

    }


    @Override
    public Bill cursorToEntity(Cursor cursor) {



        long id=  cursor.getLong(cursor.getColumnIndexOrThrow(TableBilling.TAG_ID));
        String date=cursor.getString(cursor.getColumnIndexOrThrow(TableBilling.TAG_DATE));
        String name=cursor.getString(cursor.getColumnIndexOrThrow(TableBilling.TAG_CUSTOMER_NAME));
        String bag=cursor.getString(cursor.getColumnIndexOrThrow(TableBilling.TAG_BAG       ));
        String time=cursor.getString(cursor.getColumnIndexOrThrow(TableBilling.TAG_time ));
        String des=cursor.getString(cursor.getColumnIndexOrThrow(TableBilling.TAG_DESCRIPTION        ));
        double lat= cursor.getDouble(cursor.getColumnIndexOrThrow(TableBilling.TAG_LAT));
        double Longitude= cursor.getDouble(cursor.getColumnIndexOrThrow(TableBilling.TAG_LONG));

        Bill Bill = new Bill(id,date,name,bag,time,des,lat,Longitude);


        return Bill;
    }

    @Override
    public ContentValues entityToContentValue(Bill entity) {
        ContentValues values = new ContentValues();

        values.put(TableBilling.TAG_DATE, entity.getDate());
        values.put(TableBilling.TAG_CUSTOMER_NAME, entity.getCustomer());
        values.put(TableBilling.TAG_BAG, entity.getBag());
        values.put(TableBilling.TAG_time, entity.getTime());
        values.put(TableBilling.TAG_DESCRIPTION, entity.getDes());
        values.put(TableBilling.TAG_LAT, entity.getLatitude());
        values.put(TableBilling.TAG_LONG, entity.getLongitude());


        return values;
    }



    @Override
    protected BaseContentProvider createContentProvider() {
        return new BillContentProvider();
    }


    @Override
    public synchronized Bill insert(Bill entity) {
        return super.insert(entity);

    }

    public Bill isEntryAllradyExsit (Bill toCheck){

        try {
            Bill msg = readEntityByID(toCheck.getId());
            if (msg != null){
                return msg;
            }
        } catch (Exception e) {
            return null;
        }


        return null;

    }

    public Bill insertOrUpdate (Bill toAdd) {
        Bill check = isEntryAllradyExsit(toAdd);
        if (check != null){
            return update(check.getId(), toAdd);
        }else{
            return insert(toAdd);
        }
    }
}

