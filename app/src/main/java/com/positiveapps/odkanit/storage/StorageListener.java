package com.positiveapps.odkanit.storage;



public interface StorageListener {
	
	public void onDataStorageReceived(String type, Object object);
	public void DataNotFound(String type);

}
