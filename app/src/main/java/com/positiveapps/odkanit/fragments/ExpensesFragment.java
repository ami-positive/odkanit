package com.positiveapps.odkanit.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.main.SearchActivity2;
import com.positiveapps.odkanit.network.requests.AppendExpensePost;
import com.positiveapps.odkanit.network.response.ActionResult;
import com.positiveapps.odkanit.objects.TextBox;
import com.positiveapps.odkanit.util.BitmapUtil;
import com.positiveapps.odkanit.util.DateUtil;
import com.positiveapps.odkanit.util.Helper;
import com.positiveapps.odkanit.util.ImageLoader;
import com.positiveapps.odkanit.util.PermissionUtil;
import com.positiveapps.odkanit.util.TextUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by NIVO on 21/12/2015.
 */
public class ExpensesFragment extends BaseFragment implements View.OnClickListener {


    private static final int REQUEST_CAMERA = 2;
    private static final int REQUEST_STORAGE = 3;
    private ImageView takePhotoButton;
    private ImageView photo;
    private static TextBox textBoxDate2;
    private TextBox textBoxCustomer2;
    private TextBox textBoxBag2;
    private TextBox textBoxPay;
    private TextBox textBoxDes2;
    private TextBox textBoxEsmachta;
    private ImageButton saveExpense;
    private Uri cameraFileUri;
    private String imageBase64String="";
    private ImageView customerArrow;
    private ImageView tikArrow;
    private String clientNumber;
    private static Date choosenDate;
    private ImageView deletePhoto;


    public ExpensesFragment() {
    }


    public static ExpensesFragment newInstance() {
        ExpensesFragment fragment = new ExpensesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_expenses, container, false);    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViews(view);
        setKeyBoardStuff();
        setListneres();

        initUi();
    }

    private void initUi(){
//        textBoxCustomer2.setMainText(MyApp.generalSettings.getClientWcf());
//        textBoxBag2     .setMainText(MyApp.generalSettings.getBagWcf());
        textBoxDes2     .setMainText(MyApp.generalSettings.getDesEx());
//        MyApp.generalSettings.setPayEx(textBoxPay.getMainText());
//        MyApp.generalSettings.setEsmachtaEx(textBoxEsmachta.getMainText());
        textBoxEsmachta.setMainText(MyApp.generalSettings.getEsmachtaEx());
        textBoxPay.setMainText(MyApp.generalSettings.getPayEx());

        long d= MyApp.generalSettings.getDateEx();

        if (d==0){
            textBoxDate2.setMainText(DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE));

        }else {
            textBoxDate2.setMainText(DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE,d));

        }
    }
    @Override
    public void onPause() {
        super.onPause();
        long d= DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE, textBoxDate2.getMainText());
//        MyApp.generalSettings.setTempDate(d);
        if (MyApp.generalSettings.getDateEx()!=0) {
            MyApp.generalSettings.setDateEx(d);

        }
        MyApp.generalSettings.setTempDate(d);
//        MyApp.generalSettings.setDateEx(d);
        MyApp.generalSettings.setDesEx(textBoxDes2.getMainText());
        MyApp.generalSettings.setClientEx(textBoxCustomer2.getMainText());
        MyApp.generalSettings.setBagEx(textBoxBag2.getMainText());
        MyApp.generalSettings.setPayEx(textBoxPay.getMainText());
        MyApp.generalSettings.setEsmachtaEx(textBoxEsmachta.getMainText());

    }




    private void setKeyBoardStuff() {
        textBoxPay.getMainEditText().setRawInputType(Configuration.KEYBOARD_12KEY);


        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    private void findViews(View view) {
        //find views:
        takePhotoButton= (ImageView)view.findViewById(R.id.takePhotoButton);
        textBoxDate2        =(TextBox)view.findViewById(R.id.TextBoxDate2);
        textBoxCustomer2    =(TextBox)view.findViewById(R.id.TextBoxCustomer2);
        textBoxBag2         =(TextBox)view.findViewById(R.id.TextBoxBag2);
        textBoxPay          =(TextBox)view.findViewById(R.id.TextBoxPay);
        textBoxDes2         =(TextBox)view.findViewById(R.id.TextBoxDes2);
        textBoxEsmachta     =(TextBox)view.findViewById(R.id.TextBoxEsmachta);
        saveExpense         = (ImageButton) view.findViewById(R.id.saveExpense);
        customerArrow   = (ImageView)view.findViewById(R.id.customerArrow);
        tikArrow        = (ImageView)view.findViewById(R.id.tikArrow);
        photo        = (ImageView)view.findViewById(R.id.photo);
        deletePhoto        = (ImageView)view.findViewById(R.id.deletePhoto);
    }

    private void setListneres() {
        saveExpense             .setOnClickListener(this);
        takePhotoButton .setOnClickListener(this);
        customerArrow.setOnClickListener(this);
        tikArrow.setOnClickListener(this);

        textBoxDate2            .setOnViewClickListener(new TextBoxsClickListener(textBoxDate2));
        textBoxCustomer2        .setOnViewClickListener(new TextBoxsClickListener(textBoxCustomer2));
        textBoxBag2             .setOnViewClickListener(new TextBoxsClickListener(textBoxBag2));
//        textBoxDes2             .setOnViewClickListener(new TextBoxsClickListener(textBoxDes2));

         textBoxDate2     .getEditText().setOnClickListener(new TextBoxsClickListener(textBoxDate2));
         textBoxCustomer2 .getEditText().setOnClickListener(new TextBoxsClickListener(textBoxCustomer2 ));
         textBoxBag2      .getEditText().setOnClickListener(new TextBoxsClickListener(textBoxBag2));
//         textBoxDes2      .getEditText().setOnClickListener(new TextBoxsClickListener(textBoxDes2));
        deletePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photo.setImageResource(0);
                imageBase64String = "";
                deletePhoto.setVisibility(View.INVISIBLE);


            }
        });

    }

    public class TextBoxsClickListener implements View.OnClickListener{

        View parent;
        public TextBoxsClickListener (View parent) {
            this.parent = parent;
        }

        @Override
        public void onClick(View v) {
            textBoxDes2.removeFocus();
            switch (parent.getId()){
                case R.id.TextBoxCustomer2:
                    Intent i = new Intent(getActivity(), SearchActivity2.class);
                    startActivityForResult(i, 0);
                    break;
                case R.id.TextBoxBag2:
                        Intent i2 = new Intent(getActivity(), SearchActivity2.class);
                        i2.setAction(SearchActivity2.ACTION_BAG);
                        startActivityForResult(i2, 1);
                    break;
                case R.id.TextBoxDate2:
                    showDatePickerDialog();
                    break;
                case R.id.TextBoxDes2:


                    break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MyApp.appPreference.getGeneralSettings().getIsExpenseSaved()) {
            MyApp.appPreference.getGeneralSettings().setIsExpenseSaved(false);
        }else {
            textBoxCustomer2.setMainText(MyApp.generalSettings.getClientWcf());
            textBoxBag2.setMainText(MyApp.generalSettings.getBagWcf());

        }
        String name = MyApp.generalSettings.getTempClientName();
        if (!textBoxBag2.getMainText().equals("")&&textBoxCustomer2.getMainText().equals("")){
            textBoxCustomer2.setMainText(name);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.takePhotoButton:
                if (Build.VERSION.SDK_INT > 22) {
                    checkStoragePermisson();
                }else {
                    openDialog();
                }

                break;
            case R.id.saveExpense:

                if ( TextUtil.baseFieldsValidation(textBoxCustomer2.getEditText(),
                    textBoxBag2.getEditText(),textBoxPay.getEditText(),
                    /*textBoxEsmachta.getEditText(),*/textBoxDes2.getEditText(),
                    textBoxDate2.getEditText())) {
                    String customerName = textBoxCustomer2.getMainText();
                    String bag = textBoxBag2.getMainText();
                    String des = textBoxDes2.getMainText();
                    String pay = textBoxPay.getMainText();
                    String Esmachta = textBoxEsmachta.getMainText();
//
                    choosenDate=new Date(DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE,textBoxDate2.getMainText()));
//

                    String bagNumber = MyApp.generalSettings.getBagNumberEX();
                    if (textBoxBag2.getMainText().equals("")) {
                        bagNumber = "";
                    }
                    System.out.println("tik number= "+bagNumber);

                    double p = 0.0;
                    try{
                        p = Double.parseDouble(pay);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
//                    String img = imageBase64String;



                    sendToServerOnPost(bagNumber + "", choosenDate, p, des, Esmachta + "", imageBase64String + "");

                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
                break;
            case R.id.customerArrow:
                Intent i3 = new Intent(getActivity(), SearchActivity2.class);
                startActivity(i3);
                break;
            case R.id.tikArrow:
                    Intent i4 = new Intent(getActivity(), SearchActivity2.class);
                    i4.setAction(SearchActivity2.ACTION_BAG);
                    startActivity(i4);

                break;
        }
    }

//       private void sendToServer(String TikNumber, Date date,double amount, String des,
//                                 String refre, String image) {
//           showProgressDialog();
////           ToastUtil.toaster("TikNumber "+TikNumber+",date="+date+",amount="+amount+",des="+des
////                   +",refre= "+refre
////                   , true);
//
//           MyApp.networkManager.initRestAdapter().appendExpense(
//                   new AppendExpense(MyApp.userProfile.getUserId(), TikNumber,
//                           DateUtil.convertDateToXml(date)/*date*/,
//                           amount, des, refre,
//                           "").bodyMap(),
//                   new Callback<ActionResult>() {
//                       @Override
//                       public void success(ActionResult appendExpense, Response response) {
//                           dismisProgressDialog();
//                           if (appendExpense.getSuccess()) {
//                               textBoxCustomer2.setMainText("");
//                               textBoxDes2.setMainText("");
//                               textBoxPay.setMainText("");
//                               textBoxEsmachta.setMainText("");
//                               MyApp.generalSettings.setDateWcf(0);
//                               ToastUtil.toaster(getString(R.string.report_send_sucsses), false);
//                               MyApp.appPreference.getGeneralSettings().setIsExpenseSaved(true);
//                               getActivity().finish();
//                           } else {
//                               ToastUtil.toaster(appendExpense.getErrorMessage(), true);
//                           }
//                       }
//
//                       @Override
//                       public void failure(RetrofitError error) {
//                           dismisProgressDialog();
//                           ToastUtil.toaster(error.toString(), true);
//                       }
//                   });
//       }
    public void sendToServerOnPost(String TikNumber, Date date,double amount, String des,
                                   String refre, String image) {
        showProgressDialog();
//        char slashsDate = '\'';

//        try {
//            slashsDate = URLEncoder.encode("\\", "utf-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        String newDateToSend = slashsDate+"/Date(" + date.getTime()+")"+slashsDate+"/";
        DateTime dt = new DateTime(date);
        int days =dt.getDayOfMonth();
        int month =dt.getMonthOfYear();
        int year= dt.getYear();

        String newDt = days+"-"+month+"-"+year;
//        byte b = '/';
        String newDateToSend = "/Date(" + (date.getTime()+43200000)+")"+"/";
//        String newDateToSend =  '\\'+"/Date(" + newDt+")"+'\\'+"/";
//        URLEncoder.encode("harrt poter", "utf-8")

        MyApp.networkManager.initRestAdapter().appendExpensePost(new
                AppendExpensePost(MyApp.userProfile.getUserId(), TikNumber,
                /*DateUtil.convertDateToXml(date)*/newDateToSend,
                amount, des, refre,
                image).bodyMap(), new Callback<ActionResult>() {

            @Override
            public void success(ActionResult actionResult, Response response) {
                dismisProgressDialog();
                if (actionResult.getSuccess()) {
                    textBoxCustomer2.setMainText("");
                    textBoxDes2.setMainText("");
                    textBoxPay.setMainText("");
                    textBoxEsmachta.setMainText("");
//                    MyApp.generalSettings.setDateWcf(0);
                    MyApp.generalSettings.setDateEx(0);
                    ToastUtil.toaster(getString(R.string.report_send_sucsses), false);
                    MyApp.appPreference.getGeneralSettings().setIsExpenseSaved(true);
                    getActivity().finish();
                } else {
                    ToastUtil.toaster(actionResult.getErrorMessage(), true);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismisProgressDialog();
                ToastUtil.toaster(error.toString(), true);

            }
        });
    }


    private void openDialog() {




        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.choose_photo_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialogView.findViewById(R.id.buttonGalleryDialog).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Helper.openGallery(ExpensesFragment.this);

                //your business logic
                deleteDialog.dismiss();
            }
        });
        deleteDialogView.findViewById(R.id.buttonCameraDialog).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT > 22) {
                    checkCameraPermission(deleteDialog);
                }else {
                    Helper.openCamera(ExpensesFragment.this);
                    deleteDialog.dismiss();
                }

            }
        });

        deleteDialog.show();









    }

    private void checkStoragePermisson() {
        Log.i(TAG, "Show camera button pressed. Checking permission.");
        // BEGIN_INCLUDE(camera_permission)
        // Check if the Camera permission is already available.
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED&&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

            requestStoragePermission();

//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
//                    REQUEST_CAMERA);
        } else {

            // Camera permissions is already available, show the camera preview.
            Log.i(TAG,
                    "CAMERA permission has already been granted. Displaying camera preview.");
            openDialog();

        }
        // END_INCLUDE(camera_permission)
    }

    private void requestStoragePermission() {
        if (shouldShowRequestPermissionRationale(
                Manifest.permission.READ_EXTERNAL_STORAGE)&&
                shouldShowRequestPermissionRationale(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.

            requestPermissions( new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);

        } else {

            // Camera permission has not been granted yet. Request it directly.
            requestPermissions( new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        }
        // END_INCLUDE(camera_permission_request)
    }

    private void checkCameraPermission(AlertDialog deleteDialog) {
        Log.i(TAG, "Show camera button pressed. Checking permission.");
        // BEGIN_INCLUDE(camera_permission)
        // Check if the Camera permission is already available.
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.

           requestCameraPermission();

//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
//                    REQUEST_CAMERA);
        } else {

            // Camera permissions is already available, show the camera preview.
            Log.i(TAG,
                    "CAMERA permission has already been granted. Displaying camera preview.");
            Helper.openCamera(ExpensesFragment.this);
            deleteDialog.dismiss();
        }
        // END_INCLUDE(camera_permission)
    }

    private void requestCameraPermission() {
        Log.i(TAG, "CAMERA permission has NOT been granted. Requesting permission.");
//        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
//                REQUEST_CAMERA);
        // BEGIN_INCLUDE(camera_permission_request)
        if (shouldShowRequestPermissionRationale(
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.

            requestPermissions( new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);

        } else {

            // Camera permission has not been granted yet. Request it directly.
           requestPermissions(new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
        // END_INCLUDE(camera_permission_request)
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        System.out.println("onRequestPermissionsResult");
        if (requestCode == REQUEST_CAMERA) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            Log.i(TAG, "Received response for Camera permission request.");
            System.out.println("nRequestPermissionsResult");
            // Check if the only required permission has been granted
            if (PermissionUtil.verifyPermissions(grantResults)) {

                // Camera permission has been granted, preview can be displayed
                Log.i(TAG, "CAMERA permission has now been granted. Showing preview.");
                Helper.openCamera(getActivity());

            } else {

                ToastUtil.toaster("no camera permission, you can accept permission in app setting",true);

            }
            // END_INCLUDE(permission_result)

        }else  if (requestCode == REQUEST_STORAGE){
            System.out.println("onRequestPermissionsResult");
            // Check if the only required permission has been granted
            if (PermissionUtil.verifyPermissions(grantResults)) {
                System.out.println("onRequestPermissionsResult  if ");

                // Camera permission has been granted, preview can be displayed
                openDialog();

            } else {

                ToastUtil.toaster("no storage permission, you can accept permission in app setting",true);

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap image = null;
        File fileToChnage ;
        if(resultCode != -1){
            return;
        }
        switch (requestCode){
            case Helper.CAMERA_REQUEST_CODE:
                File file = new File(Environment.getExternalStorageDirectory().getPath(), Helper.CAMERA_PROFILE_PATH);
                try {

                    shrinkImageFile(file);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                image = BitmapUtil.loadImageIntoByStringPath(file.getAbsolutePath(), photo, 500, 2, ImageLoader.DEAFULT_PLACE_HOLDER);
//                Uri u = Uri.parse(file.getAbsolutePath());
//                ImageLoader.rotate(ImageLoader.IMAGE_TYPE_FROM_CAMERA,u , image);
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(image,image.getWidth(),image.getHeight(),true);

                Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);
                photo.setImageBitmap(rotatedBitmap);

                deletePhoto.setVisibility(View.VISIBLE);
                break;
            case Helper.GALLARY_REQUEST_CODE:
                image = ImageLoader.byUri(ImageLoader.IMAGE_TYPE_FROM_GALLERAY, data.getData(), photo);
                deletePhoto.setVisibility(View.VISIBLE);
//                Uri u = data.getData();
//                ImageLoader.getPathOfImagUri(u);
//                fileToChnage = new File(u.getPath());
//
//                try {
//                    System.out.println("niv before g " + fileToChnage.length() + "");
//
//                    shrinkImageFile(fileToChnage);
//                    System.out.println("niv after g " + fileToChnage.length() + "");
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                break;



        }

//        Bitmap b = getResizedBitmap(image,500);
//        System.out.println(""+b.getHeight()+" and w"+b.getWidth());
        System.out.println(image.getByteCount()+"");
//        System.out.println ("" + b.getByteCount()+"");
//        Bitmap b  = BitmapUtil.crupAndScale()


        imageBase64String = Helper.getProfileImageAsBase64(image);



    }
    private void shrinkImageFile(File file) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
        int quality = 90;

        while (file.length() > 524288 )
        {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out);
            System.out.println("file length: " + file.length());
            if(quality <= 10){
                quality = 10;
                break;
            } else{
                quality -= 10;
            }
        }
    }


    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            int monthCorrect= month+1;
            textBoxDate2.setMainText(day+"/"+monthCorrect+"/"+year);
//            choosenDate= new Date(year,month,day);
            DateTime dateTime = new DateTime(year,monthCorrect,day,0,0);
            choosenDate = dateTime.toDate();
            MyApp.generalSettings.setTempDate(choosenDate.getTime());
            MyApp.generalSettings.setDateEx(choosenDate.getTime());

        }
    }
}
