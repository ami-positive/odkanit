package com.positiveapps.odkanit.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.positiveapps.odkanit.adapters.BagCursorAdapter;
import com.positiveapps.odkanit.database.tables.TableTik;
import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.main.SearchActivity2;
import com.positiveapps.odkanit.models.Tik;
import com.positiveapps.odkanit.network.requests.TiksList;
import com.positiveapps.odkanit.network.response.TikItem;
import com.positiveapps.odkanit.objects.onEditTextTypeTiks;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by NIVO on 24/12/2015.
 */
public class SearchBagFragment extends BaseFragment implements onEditTextTypeTiks, AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {


    private SearchBagFragmentListener listener;
    private ListView bagList;
    private BagCursorAdapter adapter;

    public SearchBagFragment() {
    }


    public static SearchBagFragment newInstance() {
        SearchBagFragment fragment = new SearchBagFragment();
        return fragment;
    }




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // try to set the activity as the listener:
        try {
            listener = (SearchBagFragmentListener) activity;
        } catch (ClassCastException e) {
            // the activity does not implement FragmentListListener!
            throw new ClassCastException(activity.toString()
                    + " must implement SearchBagFragmentListener!");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SearchActivity2.tikSearch = true;
        return inflater.inflate(R.layout.fragment_bag_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(isAdded()){
            ((SearchActivity2)getBaseActivity()).setTextTypeCallback(this);
        }

        bagList= (ListView) view.findViewById(R.id.bagList);
        setupTikListView();
        initBagsLoader();



    }
    @Override
    public void onType(String s) {
        loadTiksFromServer(s);
    }


        private void loadTiksFromServer(String s) {

            String client= MyApp.generalSettings.getTempClient();
//            showProgressDialog();
        MyApp.networkManager.initRestAdapter().tiksList(
                new TiksList(MyApp.userProfile.getUserId(), client, s).bodyMap(), new Callback<ArrayList<TikItem>>() {
                    @Override
                    public void success(ArrayList<TikItem> tiksList, Response response) {
                        if (tiksList!=null) {
                            addResultsToTable(tiksList);
//                            dismisProgressDialog();

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ToastUtil.toaster(error.toString(), true);
//                        dismisProgressDialog();

                    }
                });


}

    private void addResultsToTable(ArrayList<TikItem> tiksList) {
        MyApp.tableTik.deleteAll();


            for (int i = 0; i < tiksList.size(); i++) {

                Tik tik = new Tik(tiksList.get(i).getName(), tiksList.get(i).getNumber());
                MyApp.tableTik.insertOrUpdate(tik);

            }
            adapter.notifyDataSetChanged();


    }


    private void initBagsLoader() {
        getLoaderManager().initLoader(0, null, this);
    }


    private void setupTikListView () {
        adapter = new BagCursorAdapter(getActivity(),null);
        bagList.setAdapter(adapter);
        bagList.setOnItemClickListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return MyApp.tableTik.readLouderCursor(TableTik.TAG_ID + " ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        adapter.swapCursor(null);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Tik tik = MyApp.tableTik.readEntityByID(id);
        String name =tik.getName();
        String number= tik.getNumber();
        MyApp.generalSettings.setBagNumberWcf(number);
        MyApp.generalSettings.setBagNumberEX(number);
        MyApp.generalSettings.setBagWcf(name);
        getActivity().finish();
    }


    public interface SearchBagFragmentListener {

        void onBagSelected (String bagName, String num);
    }

}
