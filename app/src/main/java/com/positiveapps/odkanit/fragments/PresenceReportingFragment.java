package com.positiveapps.odkanit.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.network.requests.GetWorkHoursCodesList;
import com.positiveapps.odkanit.network.requests.NotifyWorkHours;
import com.positiveapps.odkanit.network.response.ActionResult;
import com.positiveapps.odkanit.network.response.WorkHoursCodeItem;
import com.positiveapps.odkanit.util.DateUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Niv on 12/22/2015.
 */
public class PresenceReportingFragment extends BaseFragment implements View.OnClickListener {


    private static TextView textViewPresence;
    private LinearLayout startDayButton;
    private ImageButton AbsenceButton;
    private TextView dateText2;
    private TextView hourText2;
    private LinearLayout endDayButton;
    private TextView dateText3;
    private TextView hourText3;
    private ImageButton miluimButton;
    private ImageButton hufshaButton;
    private ImageButton mahalaButton;
    private ImageButton kenesButton ;
    private ImageButton noPayButton ;

    private boolean AbsenceButtonCLicked=false;
    private Button leftDateButton;
    private Button rightDateButton;
    private ArrayList<WorkHoursCodeItem> arrayList;
    int countMinus =0;
//    int countPlus =10;
    private ArrayList<DateTime> dateArr2;
    private int counter=0;
    private static final int START_DAY_CODE=1;
    private static final int END_DAY_CODE =2 ;
    private static int MAHALA_CODE   ;
    private static int HUFSHA_CODE;
    private static int MILUIM_CODE;
    private ImageView checkIcon;
    private MenuItem backToToday;
    private int KENES_CODE ;
    private int NO_PAY_CODE;
    private FrameLayout button3;
    private FrameLayout button4;
    private FrameLayout button5;
    private FrameLayout button6;
    private FrameLayout button7;
    private TextView text3;
    private TextView text4;
    private TextView text5;
    private TextView text6;
    private TextView text7;


    public PresenceReportingFragment() {
    }


    public static PresenceReportingFragment newInstance() {
        PresenceReportingFragment fragment = new PresenceReportingFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_presence_reporting, container, false);



    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
        setCodes();
        setListeners();
        setHasOptionsMenu(true);
       //setCodes();

    }

    private void setUpDates() {
        Date juDate = new Date();
        DateTime dt = new DateTime(juDate);
        dateArr2= new ArrayList<>();
        for (int i=0;i<10;i++){
            DateTime date2 = dt.minusDays(i);
            dateArr2.add(date2);
        }
        DateTime now = dateArr2.get(0);
        String text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, now.getMillis());
        textViewPresence.setText(text);

        String time = DateUtil.getTimeAsStringByMilli(DateUtil.FORMAT_JUST_TIME, System.currentTimeMillis());
        String date= DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE);
        dateText2.setText(date);
        hourText2.setText(time);

    }

    private void findViews(View view) {
        textViewPresence=(TextView)view.findViewById(R.id.textViewPresence);
        startDayButton=(LinearLayout)view.findViewById(R.id.startDayButton);
        endDayButton=(LinearLayout)view.findViewById(R.id.endDayButton);
        AbsenceButton=(ImageButton)view.findViewById(R.id.AbsenceButton);
        dateText2=(TextView)view.findViewById(R.id.dateText2);
        hourText2 = (TextView) view.findViewById(R.id.hourText2);
        dateText3=(TextView)view.findViewById(R.id.dateText3);
        hourText3 = (TextView) view.findViewById(R.id.hourText3);

//        miluimButton= (ImageButton)view.findViewById(R.id.miluimButton);
//        hufshaButton= (ImageButton)view.findViewById(R.id.hufshaButton);
//        mahalaButton= (ImageButton)view.findViewById(R.id.mahalaButton);
//        kenesButton = (ImageButton)view.findViewById(R.id.kenesButton);
//        noPayButton = (ImageButton)view.findViewById(R.id.noPayButton);
        leftDateButton= (Button)view.findViewById(R.id.leftDateButton);
        rightDateButton= (Button)view.findViewById(R.id.rightDateButton);
        checkIcon= (ImageView) view.findViewById(R.id.checkIcon);

        button3= (FrameLayout)view.findViewById(R.id.button3);
        button4= (FrameLayout)view.findViewById(R.id.button4);
        button5= (FrameLayout)view.findViewById(R.id.button5);
        button6= (FrameLayout)view.findViewById(R.id.button6);
        button7= (FrameLayout)view.findViewById(R.id.button7);

        text3 = (TextView) view.findViewById(R.id.text3);
        text4 = (TextView) view.findViewById(R.id.text4);
        text5 = (TextView) view.findViewById(R.id.text5);
        text6 = (TextView) view.findViewById(R.id.text6);
        text7 = (TextView) view.findViewById(R.id.text7);

    }

    private void setListeners() {


        startDayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                MyApp.generalSettings.setIssameday(DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE));
                setDatesAndTimeOnbuttons();
                AbsenceButton.setImageResource(R.drawable.headrut_grey_button3x);
                AbsenceButton.setClickable(false);

                Date d = new Date(System.currentTimeMillis());

                notifyToServer(START_DAY_CODE, d);


            }
        });
        endDayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApp.generalSettings.setIsdaystarted(false);
                endDayButton.setVisibility(View.GONE);
                startDayButton.setVisibility(View.VISIBLE);
                Date d = new Date(System.currentTimeMillis());

                notifyToServer(END_DAY_CODE, d);

            }
        });


        AbsenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDayButton.setBackgroundResource(R.drawable.rishum_main_grey_button3x);
                startDayButton.setClickable(false);

                if (!AbsenceButtonCLicked) {
//                    showButtons();
                    showCodedButtons();
                }else {
                    invisibleTheButtons(0);
                }
            }
        });

        leftDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDayMore();

            }
        });
        rightDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDayLess();
            }
        });

    }

    private void showCodedButtons(){
        AbsenceButtonCLicked= true;

        switch (arrayList.size()-2){
            case 1:
                button5.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button5));
                text5.setText(arrayList.get(2).getName());

                break;
            case 2:
                button4.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button4));
                button6.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button6));
                text4.setText(arrayList.get(2).getName());
                text6.setText(arrayList.get(3).getName());


                break;
            case 3:
                button3.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button3));
                button5.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button5));
                button7.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button7));
                text3.setText(arrayList.get(2).getName());
                text5.setText(arrayList.get(3).getName());
                text7.setText(arrayList.get(4).getName());


                break;
            case 4:

                button3.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button3));
                button4.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button4));
                button6.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button6));
                button7.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button7));

                text3.setText(arrayList.get(2).getName());
                text4.setText(arrayList.get(3).getName());
                text6.setText(arrayList.get(4).getName());
                text7.setText(arrayList.get(5).getName());

                break;
            case 5:
                button3.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button3));
                button4.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button4));
                button5.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button5));
                button6.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button6));
                button7.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(button7));

                text3.setText(arrayList.get(2).getName());
                text4.setText(arrayList.get(3).getName());
                text5.setText(arrayList.get(4).getName());
                text6.setText(arrayList.get(5).getName());
                text7.setText(arrayList.get(6).getName());

                break;
        }

        text3.setOnClickListener(this);
        text4.setOnClickListener(this);
        text5.setOnClickListener(this);
        text6.setOnClickListener(this);
        text7.setOnClickListener(this);


    }


//    private void showButtons() {
//
//
//
//
//        AbsenceButtonCLicked= true;
//
//
////        miluimButton.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(miluimButton));
////        hufshaButton.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(hufshaButton));
////        mahalaButton.animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(mahalaButton));
////        kenesButton .animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(kenesButton ));
////        noPayButton .animate().alpha(1).setDuration(500).withEndAction(setEndActionVisible(noPayButton ));
////
//
//        miluimButton.setOnClickListener(this);
//        hufshaButton.setOnClickListener(this);
//        mahalaButton.setOnClickListener(this);
//        kenesButton .setOnClickListener(this);
//        noPayButton .setOnClickListener(this);
//
//
//    }

    private void setCodes(){
        showProgressDialog();
        MyApp.networkManager.initRestAdapter().getWorkHoursCodesList(new GetWorkHoursCodesList(MyApp.userProfile.getUserId()).bodyMap(), new Callback<ArrayList<WorkHoursCodeItem>>() {
            @Override
            public void success(ArrayList<WorkHoursCodeItem> getWorkHoursCodesList, Response response) {
//                ToastUtil.toaster(getWorkHoursCodesList.toString(), true);
                arrayList = getWorkHoursCodesList;

//                MyApp.storageManager.writeToStorage("CodesList",getWorkHoursCodesList);
//
//                System.out.println("arrayList:"+ arrayList.size()+" "+arrayList.get(2)+arrayList.get(3)
//                        +arrayList.get(4));

                dismisProgressDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                ToastUtil.toaster(error.toString(), true);
                dismisProgressDialog();
            }
        });

}


    private void notifyToServer( final int notifycode, Date date){
        showProgressDialog();
        int geoLat=0;
        int geoLong=0;
        try {
            double lat = Double.parseDouble(MyApp.appPreference.getGeneralSettings().getLat());
            double lng=  Double.parseDouble(MyApp.appPreference.getGeneralSettings().getLon());
            geoLat  = (int) lat;
            geoLong = (int) lng;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }




        MyApp.networkManager.initRestAdapter().notifyWorkHours(new NotifyWorkHours(MyApp.userProfile.getUserId(), notifycode,new DateTime(date).toString(), geoLong, geoLat).bodyMap(), new Callback<ActionResult>() {
            @Override
            public void success(ActionResult notifyWorkHours, Response response) {

                dismisProgressDialog();
                if (notifyWorkHours.getSuccess()) {
                    ToastUtil.toaster(getString(R.string.report_send_sucsses), false);
                    if (notifycode>2) {
                        checkIcon.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(closeActivityOnFinish(), 1800);
                    }


                } else {
                    ToastUtil.toaster(notifyWorkHours.getErrorMessage(), false);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismisProgressDialog();
                ToastUtil.toaster(error.toString(), true);
            }
        });
    }

//    private void closeActivity() {
//        new Runnable(){
//
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(3000);                 //1000 milliseconds is one second.
//                } catch(InterruptedException ex) {
//                    Thread.currentThread().interrupt();
//                }
//                getActivity().finish();
//
//            }
//        };
//
//    }
    private Runnable closeActivityOnFinish(){
        return new Runnable() {
            @Override
            public void run() {
                try {
                    getActivity().finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public void onClick(View v) {

        Date date= dateArr2.get(counter).toDate();
        String textClicked= null;
        switch (v.getId()){
            case R.id.text3:
                textClicked= text3.getText().toString();
                break;
            case R.id.text4:
                textClicked= text4.getText().toString();

                break;
            case R.id.text5:
                textClicked= text5.getText().toString();

                break;
            case R.id.text6:
                textClicked= text6.getText().toString();

                break;
            case R.id.text7:
                textClicked= text7.getText().toString();

                break;


        }
        if (textClicked!=null){
        int code=-1;
        for (WorkHoursCodeItem gwhl :arrayList){
            if (gwhl.getName().equals(textClicked)){
                code=gwhl.getCode();
            }
        }
            if (code!=-1)
            notifyToServer(code, date);
                invisibleTheButtons(code);
            System.out.println("textClicked:" + textClicked + " code=" + code);

        }


//        switch (v.getId()){
//            case R.id.miluimButton:
//                notifyToServer(MILUIM_CODE, date);
//                invisibleTheButtons(3);
//
//                break;
//            case R.id.hufshaButton:
//                invisibleTheButtons(HUFSHA_CODE);
//                notifyToServer(4, date);
//
//                break;
//            case R.id.mahalaButton:
//                notifyToServer(MAHALA_CODE, date);
//                invisibleTheButtons(5);
//                break;
//            case R.id.kenesButton:
//                notifyToServer(KENES_CODE, date);
//                invisibleTheButtons(6);
//                break;
//            case R.id.noPayButton:
//                notifyToServer(NO_PAY_CODE, date);
//                invisibleTheButtons(7);
//                break;
//
//
//
//        }

    }

    private void invisibleTheButtons(int button) {
        AbsenceButtonCLicked= false;

        button3.animate().alpha(0).setDuration(button == 3 ? 2000 : 500).withEndAction(setEndActionInvisible(button3));
        button4.animate().alpha(0).setDuration(button == 4 ? 2000 : 500).withEndAction(setEndActionInvisible(button4));
        button5.animate().alpha(0).setDuration(button == 5 ? 2000 : 500).withEndAction(setEndActionInvisible(button5));
        button6.animate().alpha(0).setDuration(button == 6 ? 2000 : 500).withEndAction(setEndActionInvisible(button6));
        button7.animate().alpha(0).setDuration(button == 7 ? 2000 : 500).withEndAction(setEndActionInvisible(button7));
    }

    private Runnable setEndActionInvisible(final FrameLayout button){
        return new Runnable() {
            @Override
            public void run() {
                button.setVisibility(View.INVISIBLE);
            }
        };
    }

    private Runnable setEndActionVisible(final FrameLayout button){
        return new Runnable() {
            @Override
            public void run() {
                button.setVisibility(View.VISIBLE);


            }
        };
    }



    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onResume() {
        super.onResume();

        setUpDates();
        String time = DateUtil.getTimeAsStringByMilli(DateUtil.FORMAT_JUST_TIME, System.currentTimeMillis());
        String date= DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE);

        dateText3.setText(date);
        hourText3.setText(time);

        dateText2.setText(date);
        hourText2.setText(time);


        boolean isdaystarted= MyApp.generalSettings.getIsdaystarted();
        if (isdaystarted){

            endDayButton.setVisibility(View.VISIBLE);
            startDayButton.setVisibility(View.GONE);
//            AbsenceButton.setImageResource(R.drawable.rishum_main_grey_button3x);
//            AbsenceButton.setClickable(false);
        }
        String isSameDay= MyApp.generalSettings.getIssameday();
        if (!isSameDay.equals(DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE))){
            endDayButton.setVisibility(View.GONE);
            startDayButton.setVisibility(View.VISIBLE);
        }

    }

    private void setDatesAndTimeOnbuttons() {
        MyApp.generalSettings.setIsdaystarted(true);
        String time = DateUtil.getTimeAsStringByMilli(DateUtil.FORMAT_JUST_TIME, System.currentTimeMillis());
        String date= DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE);
//        MyApp.appPreference.getGeneralSettings().setStartDayTime(time);
        //make button change:
        endDayButton.setVisibility(View.VISIBLE);
        startDayButton.setVisibility(View.GONE);
        dateText3.setText(date);
        hourText3.setText(time);

    }
    private void setDateText(int counter){
        DateTime dateTime = null;
        String text;
        switch (counter){
            case 0:
                dateTime= dateArr2.get(0);

                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                leftDateButton.setVisibility(View.INVISIBLE);
                break;
            case 1:
                dateTime= dateArr2.get(1);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                leftDateButton.setVisibility(View.VISIBLE);

                break;
            case 2:
                dateTime= dateArr2.get(2);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                break;
            case 3:
                dateTime= dateArr2.get(3);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                break;
            case 4:
                dateTime= dateArr2.get(4);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                break;
            case 5:
                dateTime= dateArr2.get(5);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                break;
            case 6:
                dateTime= dateArr2.get(6);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                break;
            case 7:
                dateTime= dateArr2.get(7);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                break;
            case 8:
                dateTime= dateArr2.get(8);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                rightDateButton.setVisibility(View.VISIBLE);
                break;
            case 9:
                dateTime= dateArr2.get(9);
                text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                rightDateButton.setVisibility(View.INVISIBLE);
                break;
        }
    }


    private void showDayLess() {

        counter++;
        setDateText(counter);
        if (counter!=0){
            startDayButton.setBackgroundResource(R.drawable.rishum_main_grey_button3x);
            startDayButton.setClickable(false);
            endDayButton.setClickable(false);
            endDayButton.setBackgroundResource(R.drawable.end_day_grey_button3x);
            backToToday.setVisible(true);

        }else {
            startDayButton.setBackgroundResource(R.drawable.start_day_button_orange);
            startDayButton.setClickable(true);
            endDayButton.setClickable(true);
            endDayButton.setBackgroundResource(R.drawable.end_day_button);
            backToToday.setVisible(false);

        }

    }

    private void showDayMore() {
        counter--;
        setDateText(counter);
        if (counter!=0){
            startDayButton.setBackgroundResource(R.drawable.rishum_main_grey_button3x);
            startDayButton.setClickable(false);
            endDayButton.setClickable(false);
            endDayButton.setBackgroundResource(R.drawable.end_day_grey_button3x);
            backToToday.setVisible(true);


        }else {
            startDayButton.setBackgroundResource(R.drawable.start_day_button_orange);
            startDayButton.setClickable(true);
            endDayButton.setClickable(true);
            endDayButton.setBackgroundResource(R.drawable.end_day_button);
            backToToday.setVisible(false);

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_presence, menu);
        MenuItem item = menu.findItem(R.id.action_today);
        backToToday= item;
        item.setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_today:
                startDayButton.setBackgroundResource(R.drawable.start_day_button_orange);
                startDayButton.setClickable(true);
                endDayButton.setClickable(true);
                endDayButton.setBackgroundResource(R.drawable.end_day_button);
                backToToday.setVisible(false);
                DateTime dateTime = dateArr2.get(0);

                String text = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, dateTime.getMillis());
                textViewPresence.setText(text);
                leftDateButton.setVisibility(View.INVISIBLE);
                rightDateButton.setVisibility(View.VISIBLE);

                counter=0;


                return true;


        }
        return false;
    }
}