package com.positiveapps.odkanit.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TimePicker;

import com.positiveapps.odkanit.main.ContactsActivity;
import com.positiveapps.odkanit.main.ExpensesActivity;
import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.main.PresenceReportingActivity;
import com.positiveapps.odkanit.main.SearchActivity2;
import com.positiveapps.odkanit.main.SummaryActivity;
import com.positiveapps.odkanit.network.requests.AppendBilling;
import com.positiveapps.odkanit.network.response.ActionResult;
import com.positiveapps.odkanit.objects.MainButtons;
import com.positiveapps.odkanit.objects.TextBox;
import com.positiveapps.odkanit.objects.TimeCounter;
import com.positiveapps.odkanit.util.DateUtil;
import com.positiveapps.odkanit.util.TextUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by NIVO on 21/12/2015.
 */
public class WriteChargesFragment extends BaseFragment implements View.OnClickListener {

    private static TextBox textBoxDate;

    private ImageButton saveReport;

    private TextBox textBoxCustomer;
    private TextBox textBoxBag;
    private MainButtons mainButtons1;
    private TextBox textBoxTime;
    private TextBox textBoxDes;
    private ImageButton timerButton;
    private ImageButton manualButton;

    public static int From_Bag_Search = 1;
    public static int From_Customer_Search = 0;
    private int state=0;
    private static Date choosenDate;

    private static TextBox hoursCounters;
    private String clientNumber;
    private String bagNumber;
    private static boolean chronometterVisible;
    private final double MINUTES_CLC= 1.6666666667;

    public WriteChargesFragment(){

    }

    public static WriteChargesFragment newInstance() {
        WriteChargesFragment fragment = new WriteChargesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_write_charges, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findviews(view);
        setListeners();

        choosenDate=new Date(System.currentTimeMillis());
        MyApp.generalSettings.setTempDate(System.currentTimeMillis());

        setMainButtonListeners(view);

        setTimeCountButton();

        initUI();

    }

    public void initUI(){

        textBoxCustomer.setMainText(MyApp.generalSettings.getClientWcf());
        textBoxBag.setMainText(MyApp.generalSettings.getBagWcf());
        textBoxDes.setMainText(MyApp.generalSettings.getDesWcf());

        long d= MyApp.generalSettings.getDateWcf();
        if (d==0){
            textBoxDate.setMainText(DateUtil.getCurrentDateAsString(DateUtil.FORMAT_JUST_DATE));

        }else {
            textBoxDate.setMainText(DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE,d));

        }

        boolean b = MyApp.generalSettings.getManual();
        if (b){
            long hours= MyApp.generalSettings.getHoursWcf();
            if (hours!=0) {
                hoursCounters.setMainText(DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_TIME, hours));
            }
            else
            {
                hoursCounters.setMainText("00:00:00");

            }
        }
    }

    private void setListeners() {
        //listeners


        textBoxCustomer .setOnViewClickListener(new TextBoxsClickListener(textBoxCustomer));
        hoursCounters    .setOnViewClickListener(new TextBoxsClickListener(hoursCounters));
        textBoxDate     .setOnViewClickListener(new TextBoxsClickListener(textBoxDate));
        textBoxBag      .setOnViewClickListener(new TextBoxsClickListener(textBoxBag));

        saveReport.setOnClickListener(this);

        textBoxCustomer.getEditText().setOnClickListener(new TextBoxsClickListener(textBoxCustomer));
        hoursCounters  .getEditText().setOnClickListener(new TextBoxsClickListener(hoursCounters));
        textBoxDate    .getEditText().setOnClickListener(new TextBoxsClickListener(textBoxDate));
        textBoxBag     .getEditText().setOnClickListener(new TextBoxsClickListener(textBoxBag));

    }

    private void setTimeCountButton() {
        MyApp.timeCount.setButtons(
                hoursCounters.getStop(),
                hoursCounters.getStart(),
                hoursCounters.getPause(),
                hoursCounters.getResume(),
                hoursCounters.getManual()
        );

        timerClickListener(hoursCounters.getStop());
        timerClickListener(hoursCounters.getStart());
        timerClickListener(hoursCounters.getPause());
        timerClickListener(hoursCounters.getResume());
        timerClickListener(hoursCounters.getManual());
    }

    private void setMainButtonListeners(View view) {
        mainButtons1 = (MainButtons) view.findViewById(R.id.MainButtons2);

        mainButtons1.sefer_telefonim_button().setOnClickListener(this);
        mainButtons1.buttonDoh_mesakem().setOnClickListener(this);
        mainButtons1.nohahut_button().setOnClickListener(this);
        mainButtons1.hozaot_button().setOnClickListener(this);
    }

    private void findviews(View view) {
        //find views:
        saveReport=(ImageButton)view.findViewById(R.id.saveReport);
        textBoxDate =(TextBox)view.findViewById(R.id.textBoxDate);
        textBoxCustomer=(TextBox)view.findViewById(R.id.textBoxCustomer);
        textBoxBag=(TextBox)view.findViewById(R.id.textBoxBag);
        textBoxDes =(TextBox)view.findViewById(R.id.TextBoxDes);
        hoursCounters=(TextBox)view.findViewById(R.id.hoursCounters);
    }


    public class TextBoxsClickListener implements View.OnClickListener{

        View parent;
        public TextBoxsClickListener (View parent) {
            this.parent = parent;
        }

        @Override
        public void onClick(View v) {
            textBoxDes.removeFocus();
            switch (parent.getId()){
                case R.id.textBoxCustomer:
                    Intent i = new Intent(getActivity(), SearchActivity2.class);
                    startActivityForResult(i, From_Customer_Search);
                    break;
                case R.id.hoursCounters:
                    showTimePickerDialog();
                    break;
                case R.id.textBoxBag:
                    Intent i2 = new Intent(getActivity(), SearchActivity2.class);
                    i2.setAction(SearchActivity2.ACTION_BAG);
                    startActivityForResult(i2, From_Bag_Search);

                    break;
                case R.id. textBoxDate:
                    //open date Picker Dialog
//                DialogManager
                    showDatePickerDialog(v);

                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.saveReport:
                if (MyApp.timeCount.getState()== 1){
                    ToastUtil.toaster(getResources().getString(R.string.not_posible_save_whie_timer),true);
                }else {
                    if (TextUtil.baseFieldsValidation(textBoxCustomer.getEditText(), textBoxBag.getEditText(),
                            textBoxDes.getEditText(), textBoxDate.getEditText())) {
                        String customerName = textBoxCustomer.getMainText();
                        double time = 0;
                        String bag = MyApp.generalSettings.getBagNumberWcf();
                        String des = textBoxDes.getMainText();
                        System.out.println("bag= "+bag);
                        double geoLat = 0.0;
                        double geoLong = 0.0;
                        try {
                            double lat = Double.parseDouble(MyApp.appPreference.getGeneralSettings().getLat());
                            double lng = Double.parseDouble(MyApp.appPreference.getGeneralSettings().getLon());
                            geoLat = lat;
                            geoLong = lng;
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        time = getAndClcTime();
                        choosenDate = new Date(MyApp.generalSettings.getTempDate());
                        sendToServer(choosenDate, customerName, time, bag, des, geoLat, geoLong);

                        View view = getActivity().getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }
                }
                break;

            case R.id.sefer_telefonim_button:
                Intent contactsIntent = new Intent(getActivity(), ContactsActivity.class);
                startActivity(contactsIntent);

                break;
            case R.id.buttonDoh_mesakem:
                Intent sumIntent= new Intent(getActivity(), SummaryActivity.class);
                startActivity(sumIntent);

                break;
            case R.id.hozaot_button:
                Intent hozaotIntent = new Intent(getActivity(), ExpensesActivity.class);
                startActivity(hozaotIntent);


                break;
            case R.id.nohahut_button:
                Intent nohehutIntent = new Intent(getActivity(), PresenceReportingActivity.class);
                startActivity(nohehutIntent);

                break;
        }
    }

    private double getAndClcTime() {
        double time;
        if (chronometterVisible) {

//                        time = ((MyApp.timeCount.getDifference() / 1000.0) / 60.0) / 60.0;
                long mils= MyApp.timeCount.getDifference();
//                        long mils= (long)((int) (Math.random()*122292818+1292818));
//                int minutes = (int) ((mils / (1000*60)) % 60);
//                int hours   = (int) ((mils / (1000*60*60)) % 24);
//            System.out.println(""+hours+minutes);

//            minutes=  clcMinutes(minutes);
//                String timeAsString= hours+"."+minutes;

            int mins = (int) mils/60000;
                String timeAsString= mins+".0";

                System.out.println("timeAsString= "+timeAsString);

                time = Double.parseDouble(timeAsString);
        } else {

//
            System.out.println("hoursCounters= " + hoursCounters.getMainText());
            String[] curretnSetTime = hoursCounters.getMainText().split(Pattern.quote(":"));

            int[] hoursAndMin = new int[2];
            if (curretnSetTime.length>1) {
                hoursAndMin[0] = Integer.parseInt(curretnSetTime[0]);
                hoursAndMin[1] = Integer.parseInt(curretnSetTime[1]);
            }
                int minutes= hoursAndMin[1];
//            minutes = clcMinutes(minutes);
            String timeAsString= hoursAndMin[0]+"."+minutes;
            /**
             * 7.4.16 change
             */
                int allMinutes = (hoursAndMin[0]*60)+minutes;
            timeAsString= allMinutes+".0";
                System.out.println("timeAsString= "+timeAsString);
                time = Double.parseDouble(timeAsString);

        }
        System.out.println("time= "+time);
        return time;
    }

    private int clcMinutes(int minutes) {
        minutes= (int)(minutes*MINUTES_CLC);
//        if (minutes>=75)
//            minutes=75;
//        else if (minutes<75&&minutes>=50)
//            minutes=50;
//        else if (minutes<50&&minutes>=25)
//            minutes=25;
//        else if (minutes<25)
//            minutes=0;
//        System.out.println(""+ minutes);
        return minutes;
    }

    private void sendToServer(Date date, String customerName, double time, String tikNumber, String des, double lat, double lon) {
//        ToastUtil.toaster("tikNumber "+tikNumber+",date="+date+",time="+time+",des="+des, true);


        showProgressDialog();

        MyApp.networkManager.initRestAdapter().appendBilling(
                new AppendBilling(
                        MyApp.userProfile.getUserId(), tikNumber, new DateTime(date).toString(), time, des, lon, lat).bodyMap(), new Callback<ActionResult>() {
                    @Override
                    public void success(ActionResult appendBilling, Response response) {
                        dismisProgressDialog();
                        if (appendBilling.getSuccess()) {
                            ToastUtil.toaster(getString(R.string.the_inf_saved), false);

                            textBoxCustomer.setMainText("");
                            textBoxBag.setMainText("");
                            textBoxDes.setMainText("");
                            hoursCounters.getMainEditText().setText("00:00:00");
                            MyApp.generalSettings.setDateEx(0);
                            MyApp.generalSettings.setHoursWcf(0);
                            MyApp.generalSettings.setClientWcf("");
                            MyApp.generalSettings.setBagWcf("");

                            getFragmentManager().popBackStack();
                        } else {
                            ToastUtil.toaster(appendBilling.getErrorMessage(), false);
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dismisProgressDialog();
                        ToastUtil.toaster(error.toString(), true);
                    }
                });



    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            int monthCurrect= month+1;
            textBoxDate.setMainText(day+"/"+monthCurrect+"/"+year);
            DateTime dateTime = new DateTime(year,monthCurrect,day,0,0);
            choosenDate = dateTime.toDate();
//            choosenDate= new Date(year,monthCurrect,day);
            MyApp.generalSettings.setTempDate(choosenDate.getTime());


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode==0) {
            String clientName= null;
            clientNumber=null;
            String bagName= null;
            bagNumber= null;

            try {
                clientName = data.getStringExtra(SearchActivity2.CLIENT_NAME);
                clientNumber=data.getStringExtra(SearchActivity2.CLIENT_NUMBER);

                bagName = data.getStringExtra(SearchActivity2.BAG_NAME);
                bagNumber = data.getStringExtra(SearchActivity2.BAG_NUMBER);


            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            switch (resultCode) {
                case 0:
                    textBoxCustomer.setMainText(clientName);
                    textBoxBag.setMainText(bagName);
                    textBoxCustomer.removeError();
                    textBoxBag.removeError();
//                    MyApp.generalSettings.setClientWcf(clientName);
//                    MyApp.generalSettings.setBagWcf(bagName);
//                    MyApp.generalSettings.setBagNumberWcf(bagNumber);

                    break;
                case 1:
//                    MyApp.generalSettings.setBagWcf(bagName);
//                    MyApp.generalSettings.setBagNumberWcf(bagNumber);
//                    textBoxCustomer.setMainText(MyApp.generalSettings.getClientWcf());
                    textBoxBag.setMainText(bagName);
                    textBoxBag.removeError();
                    break;
            }

        }

    }

    public View.OnClickListener timerClickListener(final ImageButton imageButton) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                switch (imageButton.getId()){
                    case R.id.start:
                        Log.i(TAG, "start ");
                        MyApp.timeCount.start();
                        hoursCounters.getChronometer().setBase(SystemClock.elapsedRealtime());
                        hoursCounters.getChronometer().setVisibility(View.VISIBLE);
                        hoursCounters.getMainEditText().setVisibility(View.GONE);
                        hoursCounters.getChronometer().start();
                        chronometterVisible= true;
                        MyApp.generalSettings.setManual(false);

                        break;
                    case R.id.stop:
                        Log.i(TAG, "stop ");
                        MyApp.timeCount.stop();

                        MyApp.timeCount.setTimeWhenStopped(0L);
                        hoursCounters.getChronometer().stop();
                        long count =  SystemClock.elapsedRealtime()-hoursCounters.getChronometer().getBase();
                        System.out.println("count " + count);
//                        count= hoursCounters.getChronometer().getText().toString();

                        hoursCounters.getChronometer().setText(DateUtil.getTimeAsStringByMilliForHourCounter(DateUtil.FORMAT_JUST_TIME,count));

                        System.out.println("getDifference() " + MyApp.timeCount.getDifference() / 1000);
                        chronometterVisible= true;




                        break;
                    case R.id.pause:
                        Log.i(TAG, "pause ");
                        MyApp.timeCount.pause();
                        MyApp.timeCount.setTimeWhenStopped(hoursCounters.getChronometer().getBase() - SystemClock.elapsedRealtime());
                        hoursCounters.getChronometer().stop();
                        break;
                    case R.id.resume:
                        Log.i(TAG, "resume ");
                        MyApp.timeCount.resume();
                        hoursCounters.getChronometer().setBase(SystemClock.elapsedRealtime() + MyApp.timeCount.getTimeWhenStopped());
                        hoursCounters.getChronometer().start();
                        chronometterVisible= true;

                        break;
                    case R.id.manual:
                        Log.i(TAG, "manual ");
                        hoursCounters.getChronometer().setVisibility(View.GONE);
                        hoursCounters.getMainEditText().setVisibility(View.VISIBLE);
                        chronometterVisible= false;
                        showTimePickerDialog();
                        MyApp.generalSettings.setManual(true);

                        break;
                }
            }
        };

        imageButton.setOnClickListener(listener);
        return listener;
    }

    public void initChronometer(){
        if(MyApp.timeCount.getState() == TimeCounter.STATE_RUNNING){
            hoursCounters.getChronometer().setVisibility(View.VISIBLE);
            hoursCounters.getMainEditText().setVisibility(View.GONE);
            chronometterVisible= true;
            hoursCounters.getChronometer().setBase(SystemClock.elapsedRealtime() + (MyApp.timeCount.getStartCount() - System.currentTimeMillis()));
            hoursCounters.getChronometer().start();
        }else if(MyApp.timeCount.getState() == TimeCounter.STATE_PAUSED){
            hoursCounters.getChronometer().setVisibility(View.VISIBLE);
            chronometterVisible= true;
            hoursCounters.getMainEditText().setVisibility(View.GONE);
            hoursCounters.getChronometer().setBase(SystemClock.elapsedRealtime() + MyApp.timeCount.getTimeWhenStopped());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getBaseActivity().setupActionBar(R.id.app_actionbar, getString(R.string.write_charges));
        getBaseActivity().getActionBarTextView().setTextColor(Color.WHITE);

        textBoxBag.setMainText(MyApp.generalSettings.getBagWcf());
        textBoxCustomer.setMainText(MyApp.generalSettings.getClientWcf());
        String name = MyApp.generalSettings.getTempClientName();

        if (!textBoxBag.getMainText().equals("")&&textBoxCustomer.getMainText().equals("")){
            textBoxCustomer.setMainText(name);
        }



        initChronometer();
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApp.userSettings.setTimerState(MyApp.timeCount.getState());
        MyApp.userSettings.setTimerDifference(MyApp.timeCount.getDifference());
        MyApp.userSettings.setTimerStart(MyApp.timeCount.getStartCount());
        MyApp.userSettings.setTimeWhenStopped(MyApp.timeCount.getTimeWhenStopped());
        System.out.println("MyApp.timeCount.getTimeWhenStopped() " + MyApp.timeCount.getTimeWhenStopped());
        System.out.println("MyApp.timeCount.getStartCount() 1 " + MyApp.timeCount.getStartCount());
        System.out.println("setTimerDifference " + (MyApp.timeCount.getTimeWhenStopped() - MyApp.timeCount.getStartCount()));

        long d= DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE, textBoxDate.getMainText());

        MyApp.generalSettings.setTempDate(d);
        MyApp.generalSettings.setDateWcf(d);
        MyApp.generalSettings.setDesWcf(textBoxDes.getMainText());
        MyApp.generalSettings.setClientWcf(textBoxCustomer.getMainText());
        MyApp.generalSettings.setBagWcf(textBoxBag.getMainText());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        MyApp.generalSettings.setClientWcf(textBoxCustomer.getMainText());
//        MyApp.generalSettings.setBagWcf(textBoxBag.getMainText());
    }

    public void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();

        newFragment.show(getFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String mins = null;
            String hou = null;
//            if (chronometterVisible) {

//                        time = ((MyApp.timeCount.getDifference() / 1000.0) / 60.0) / 60.0;
                long mils = MyApp.timeCount.getDifference();
//                        long mils= (long)((int) (Math.random()*122292818+1292818));
                int minutes = (int) ((mils / (1000 * 60)) % 60);
                int hours = (int) ((mils / (1000 * 60 * 60)) % 24);
                System.out.println("hours+minutes= "+hours+":"+minutes);

                if (minutes<10){
                    mins= "0"+minutes;
                }else {
                    mins= minutes+"";
                }
                if (hours<10){
                    hou= "0"+hours;
                }else {
                    hou= hours+"";
                }
                System.out.println(""+hou+":"+mins);

//            }

            SimpleDateFormat sdf = new SimpleDateFormat("hh:ss");
            Date date2 = null;
            String timeoftimer = hou+":"+mins;

            try {
                if (minutes!=0&&hours!=0)
                    date2 = sdf.parse(timeoftimer);
                else {
                    date2 = sdf.parse("01:00");
                }

            } catch (ParseException e) {
            }
            Calendar c = Calendar.getInstance();
            c.setTime(date2);


            // Use the current time as the default values for the picker
//            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(),android.R.style.Theme_Holo_Light_Dialog_NoActionBar ,this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }


        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


            String hourOfDayS=hourOfDay+"";
            String minuteS=minute+"";
            if (hourOfDay<=9){
                 hourOfDayS= "0"+hourOfDay;
            }
            if (minute<=9){
                 minuteS= "0"+minute;
            }
            hoursCounters.getMainEditText().setText(hourOfDayS + ":" + minuteS+":00");


            long date= DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_TIME,hourOfDayS + ":" + minuteS+":00");

            MyApp.generalSettings.setHoursWcf(date);

        }
    }



}

