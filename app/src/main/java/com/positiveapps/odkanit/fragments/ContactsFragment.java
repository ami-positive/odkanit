package com.positiveapps.odkanit.fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.inject.Inject;
import com.positiveapps.odkanit.adapters.ContactsCursorAdapter;
import com.positiveapps.odkanit.adapters.LastContactsCursorAdapter;
import com.positiveapps.odkanit.database.tables.Table10Contacts;
import com.positiveapps.odkanit.database.tables.TableCustomer;
import com.positiveapps.odkanit.main.ContactsActivity;
import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.models.Customer;
import com.positiveapps.odkanit.network.requests.GetPhoneBook;
import com.positiveapps.odkanit.network.response.PhoneNumberItem;
import com.positiveapps.odkanit.network.response.PhonebookItem;
import com.positiveapps.odkanit.objects.onContactsSelected;
import com.positiveapps.odkanit.util.FragmentsUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Field;
import roboguice.inject.InjectView;

/**
 * Created by Niv on 12/22/2015.
 */
public class ContactsFragment extends BaseFragment implements
        AdapterView.OnItemClickListener,LoaderManager.LoaderCallbacks<Cursor>
        ,ContactsActivity.OnSearchListener {
    private ListView listContacts;
    private ContactsCursorAdapter adapter;
    private ListView listLastContacts;
    private LastContactsCursorAdapter lastContactsAdapter;
    public onContactsSelected callback;

    @InjectView(R.id.searchContact)
    private EditText searchContact;

    @InjectView(R.id.clearSearch)
    private Button clearSearch;

    @InjectView(R.id.searchContactEditText)
    private FrameLayout searchContactEditText;

    public ContactsFragment() {
    }


    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
     }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final View viewF= view;


        listContacts= (ListView)view.findViewById(R.id.listContacts);
        listLastContacts= (ListView)view.findViewById(R.id.listLastContacts);



        setupContactsListView();
        initContactsLoader();



        ((ContactsActivity)getActivity()).setOnSearchListener(this);

        clearSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchContact.setText("");
            }
        });
        searchContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length()>1) {
                    listLastContacts.setVisibility(View.GONE);
                    listContacts.setVisibility(View.VISIBLE);
                    clearSearch.setVisibility(View.VISIBLE);
                    final int length =s.toString().length();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            if (searchContact.getText().toString().length()==length)
                            loadContactsFromServer(searchContact.getText().toString());

                        }
                    }, 500);

                }else {
                    listLastContacts.setVisibility(View.VISIBLE);
                    listContacts.setVisibility(View.GONE);
                    clearSearch.setVisibility(View.INVISIBLE);

                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        callback= ContactsActivity

        searchContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (searchContact.getText().toString().length()>0) {
                        loadContactsFromServer(searchContact.getText().toString());
                        listLastContacts.setVisibility(View.GONE);
                        listContacts.setVisibility(View.VISIBLE);
                        clearSearch.setVisibility(View.VISIBLE);
//                        View view = this.getCurrentFocus();
                        if (viewF != null) {
                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(viewF.getWindowToken(), 0);
                        }
                        return true;
                    }
                }
                if (viewF != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(viewF.getWindowToken(), 0);
                }
                return false;
            }
        });


    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // try to set the activity as the listener:
        try {
            callback = (ContactsActivity) activity;
        } catch (ClassCastException e) {
            // the activity does not implement FragmentListListener!
            throw new ClassCastException(activity.toString()
                    + " must implement SearchCustomerFragmentListener!");
        }
    }


    private void setupContactsListView() {
        adapter = new ContactsCursorAdapter(getActivity(),null);
        listContacts.setAdapter(adapter);
        listContacts.setOnItemClickListener(this);


        Cursor cursor = MyApp.table10Contacts.readCursor("",Table10Contacts.TAG_TIME + " desc");
        if (cursor.moveToFirst()) {
            lastContactsAdapter = new LastContactsCursorAdapter(getActivity(), cursor);
            listLastContacts.setAdapter(lastContactsAdapter);
            listLastContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FragmentsUtil.openFragmentFade(getFragmentManager(), ContactsDetailsFragment.newInstance(id, false), R.id.contacts_container);
                    getActivity().closeOptionsMenu();
                    callback.onContactsItemClick();


                }
            });
        }
    }

    private void loadContactsFromServer(String s) {
//        showProgressDialog();


        MyApp.networkManager.initRestAdapter().getPhoneBook(
                new GetPhoneBook(MyApp.userProfile.getUserId(), s/*searchString*/).bodyMap(),
                new Callback<ArrayList<PhonebookItem>>() {
                    @Override
                    public void success(ArrayList<PhonebookItem> getPhoneBook, Response response) {
//                        dismisProgressDialog();
                        addResultsToTable(getPhoneBook);


                    }

                    @Override
                    public void failure(RetrofitError error) {
//                        dismisProgressDialog();
                        ToastUtil.toaster(error.toString(), true);
                    }
                });



    }

//    private void setTextWatcher(EditText editText){
//        editText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (count > 1) {
//                    loadContactsFromServer(s.toString());
//                }
//                if (count>=1){
//                    clearSearch.setVisibility(View.VISIBLE);
//                }else {
//                    clearSearch.setVisibility(View.INVISIBLE);
//
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//    }

    private void addResultsToTable(ArrayList<PhonebookItem> getPhoneBook) {
        showProgressDialog();
        if (getPhoneBook!=null) {
            MyApp.tableCustomer.deleteAll();
            for (int i = 0; i < getPhoneBook.size(); i++) {
                String name= getPhoneBook.get(i).getName();
                String Type= getPhoneBook.get(i).getType();
                String address= getPhoneBook.get(i).getAddress();
                ArrayList<PhoneNumberItem> phoneNumberItems = getPhoneBook.get(i).getPhones();
                String Email= getPhoneBook.get(i).getEMail();
//                String phoneNum = phoneNumberItems.get(0).getNumber();
                String phoneNum="",phone1="",phone2="",fax="";
//                String phoneType= phoneNumberItems.get(0).getType();
//                String phoneTypeOffice= phoneNumberItems.get(1).getType();
                for (int j=0;j<phoneNumberItems.size();j++){
                    if (phoneNumberItems.get(j).getType().equals("טלפון 1")){
                      phone1 = phoneNumberItems.get(j).getNumber();
                    }else if(phoneNumberItems.get(j).getType().equals("טלפון 2")){
                        phone2= phoneNumberItems.get(j).getNumber();
                    }else if (phoneNumberItems.get(j).getType().equals("פקס")){
                        fax= phoneNumberItems.get(j).getNumber();

                    }else if (phoneNumberItems.get(j).getType().equals("נייד")){
                        phoneNum=phoneNumberItems.get(j).getNumber();
                    }
                }


                Customer customer= new Customer(name,"",Type,phoneNum,phone1,phone2,fax,Email,address);
                MyApp.tableCustomer.insertOrUpdate(customer);

            }
            adapter.notifyDataSetChanged();
            dismisProgressDialog();
        }
    }

    private void initContactsLoader() {
        getLoaderManager().initLoader(1, null, this);
        getLoaderManager().initLoader(0, null, this);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Customer c  = MyApp.tableCustomer.readEntityByID(id);
        Customer newCustomer= new Customer(c.getName(),c.getLastName(),c.getType(),c.getPhone(),c.getPhone1(),c.getPhone2(),c.getFax(),c.getEmail(),c.getAddress(),System.currentTimeMillis());
//        ArrayList<Customer> customers = MyApp.table10Contacts.readAll("");//
//        if (customers==null){

        int j=0;
        ArrayList<Customer> customers = MyApp.table10Contacts.readAll(Table10Contacts.TAG_TIME + " ASC");
        for (int i = 0; i <customers.size() ; i++) {
            Customer c3= customers.get(i);

            if (customers.get(i).getPhone().equals(newCustomer.getPhone())&&customers.get(i).getName().equals(newCustomer.getName()) ){
                j++;
            }


        }
        if (j==0) {
            MyApp.table10Contacts.insert(newCustomer);
        }
        if (customers.size()>=10) {
            Customer firstCustomer= customers.get(0);
            MyApp.table10Contacts.delete(firstCustomer.getId(),firstCustomer);
//            System.out.println("firstCustomer "+ firstCustomer.getTimeInMil());
        }


        FragmentsUtil.openFragmentFade(getFragmentManager(), ContactsDetailsFragment.newInstance(id, true), R.id.contacts_container);
        getActivity().closeOptionsMenu();

        callback.onContactsItemClick();



    }



    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().setupActionBar(R.id.app_actionbar, getString(R.string.sefer_telephonim));

        listLastContacts.setVisibility(View.VISIBLE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id){
            case 0:
                return MyApp.tableCustomer.readLouderCursor(TableCustomer.TAG_ID + " ASC");

            case 1:
//                SelectionBuilder s = new SelectionBuilder().where(Table10Contacts.TAG_ID).smallThen(10);

                return MyApp.table10Contacts.readLouderCursor( Table10Contacts.TAG_TIME + " desc");
            default:
                return MyApp.tableCustomer.readLouderCursor(TableCustomer.TAG_ID + " ASC");
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }


    @Override
    public void onNewKeyword(String keyword) {

        searchContactEditText.setVisibility(View.VISIBLE);
//        if (keyword.length() >= 2) {
//            loadContactsFromServer(keyword);
//            listLastContacts.setVisibility(View.GONE);
//            listContacts.setVisibility(View.VISIBLE);
//        }else {
//            listLastContacts.setVisibility(View.VISIBLE);
//            listContacts.setVisibility(View.GONE);
//        }
//
//        getLoaderManager().initLoader(0, null, this);
    }

//    public void setContactsSelectedCallback(onContactsSelected callback) {
//        this.callback = callback;
//    }

}