package com.positiveapps.odkanit.fragments.sidescreen;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.positiveapps.odkanit.fragments.BaseFragment;
import com.positiveapps.odkanit.fragments.MainFragment;
import com.positiveapps.odkanit.fragments.WriteChargesFragment;
import com.positiveapps.odkanit.main.ContactsActivity;
import com.positiveapps.odkanit.main.ExpensesActivity;
import com.positiveapps.odkanit.main.LogInActivity;
import com.positiveapps.odkanit.main.MainActivity;
import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.main.PresenceReportingActivity;
import com.positiveapps.odkanit.main.SummaryActivity;
import com.positiveapps.odkanit.storage.AppPreference;
import com.positiveapps.odkanit.util.AppUtil;
import com.positiveapps.odkanit.util.ContentProviderUtil;
import com.positiveapps.odkanit.util.FragmentsUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import java.io.File;


/**
 * Created by Izakos on 11/10/2015.
 */
public class SideFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private ListView SideListView;
    private int positionClicked;
    private ImageView powerdLogo;
    private TextView version;

    public SideFragment() {
        // TODO Auto-generated constructor stub
    }

    public static SideFragment newInstance() {
        SideFragment instance = new SideFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = R.layout.fragment_side;
        View rootView = inflater.inflate(layoutRes, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SideListView = (ListView) view.findViewById(R.id.SideListView);
        powerdLogo= (ImageView) view.findViewById(R.id.powerdLogo);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.side_list_item,R.id.sideItem, getResources().getStringArray(R.array.sideMenu));
        SideListView.setAdapter(adapter);

        SideListView.setOnItemClickListener(this);
        powerdLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentProviderUtil.openBrowser(getActivity(), "http://www.positive-apps.com/he");
            }
        });

        version = (TextView)view.findViewById(R.id.version);
        version.setText("Version "+AppUtil.getApplicationVersion(getActivity()));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MainActivity.mDrawerLayout.closeDrawers();
        switch (position){
            case 0:
                //rishum huuvim
                if (getBaseActivity().getCurrentFragment() instanceof MainFragment) {
                    FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
                            WriteChargesFragment.newInstance(),
                            R.id.main_container);
                }else {
                    return;
                }

                break;
            case 1:
                Intent sumIntent= new Intent(getActivity(), SummaryActivity.class);
                startActivity(sumIntent);

                break;
            case 2:

                Intent nohehutIntent = new Intent(getActivity(), PresenceReportingActivity.class);
                startActivity(nohehutIntent);

                break;
            case 3:

                Intent contactsIntent = new Intent(getActivity(), ContactsActivity.class);
                startActivity(contactsIntent);

                break;
            case 4:
                Intent i = new Intent(getActivity(), ExpensesActivity.class);
                startActivity(i);


                break;
//            case 5:
//
//                Intent settingsI = new Intent(getActivity(), SettingsActivity.class);
//                startActivity(settingsI);
//                break;
            case 5:
                MyApp.userProfile.setLoggedIn(false);
//                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
//                preferences.edit().clear().apply();
//                SharedPreferences.Editor.clear().commit()
//                SharedPreferences.Editor;
//                SharedPreferences.Editor editor = preferences.edit();
//                editor.clear();
//                editor.commit();

//                getActivity().getSharedPreferences("clear_cache", Context.MODE_PRIVATE).edit().clear().commit();
                clearSharedPreferences(getActivity());
//                editor.clear();
//                editor.commit();

//                getActivity().finish();
//                clearApplicationData();
                MyApp.deleteDatabase();

//                Intent logOutIntent = new Intent(getActivity(), LogInActivity.class);
//                startActivity(logOutIntent);


//                Intent restart = getActivity().getBaseContext().getPackageManager()
//                        .getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName() );
//                restart.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(restart);

//                Intent mStartActivity = new Intent(getContext(),  LogInActivity.class);
//                int mPendingIntentId = 123456;
//                PendingIntent mPendingIntent = PendingIntent.getActivity(getContext(), mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
//                AlarmManager mgr = (AlarmManager)getContext().getSystemService(Context.ALARM_SERVICE);
//                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
//                System.exit(0);

                Intent logOutIntent = new Intent(getActivity(), LogInActivity.class);
                logOutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                logOutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                logOutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
                startActivity(logOutIntent);


//                MyApp.generalSettings.setHoursWcf(0);
//                MyApp.generalSettings.setIsdaystarted(false);
//                MyApp.generalSettings.setDateWcf(System.currentTimeMillis());
//                MyApp.generalSettings.setBagWcf("");
//                MyApp.generalSettings.setClientWcf("");
//                MyApp.generalSettings.setDesWcf("");
//                MyApp.generalSettings.setDateEx(System.currentTimeMillis());
//                MyApp.generalSettings.setBagEx("");
//                MyApp.generalSettings.setClientEx("");
//                MyApp.generalSettings.setDesEx("");
//                MyApp.generalSettings.setPayEx(0);
//                MyApp.generalSettings.setEsmachtaEx("");
//                MyApp.userProfile.





                break;
        }

    }
    public static void clearSharedPreferences(Context ctx){
        File dir = new File(ctx.getFilesDir().getParent() + "/shared_prefs/");
        String[] children = dir.list();
        for (int i = 0; i < children.length; i++) {
            // clear each of the prefrances
            ctx.getSharedPreferences(children[i].replace(".xml", ""), Context.MODE_PRIVATE).edit().clear().commit();
        }
        // Make sure it has enough time to save all the commited changes
        try { Thread.sleep(1000); } catch (InterruptedException e) {}
        for (int i = 0; i < children.length; i++) {
            // delete the files
            new File(dir, children[i]).delete();
        }
    }
    public void clearApplicationData() {
        File cache = getBaseActivity().getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }
//    @Override
//    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        MainActivity.mDrawerLayout.closeDrawers();
//        switch (i){
//            case 2:
//                FragmentsUtil.openFragment(getFragmentManager(), ContactsFragment.newInstance(), R.id.main_container);
//                break;
//            case 3:
//                FragmentsUtil.openFragment(getFragmentManager(), UniversityFragment.newInstance(Static.URL_TUTORIAL_VID), R.id.main_container);
//                break;
//            case 4:
//                FragmentsUtil.openFragment(getFragmentManager(), ProfileFragment.newInstance(), R.id.main_container);
//                break;
//            case 5:
//                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
//                break;
//            default:
//                break;
//        }
//    }


}
