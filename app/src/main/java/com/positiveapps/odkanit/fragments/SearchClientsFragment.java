package com.positiveapps.odkanit.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.positiveapps.odkanit.adapters.ClientCursorAdapter;
import com.positiveapps.odkanit.adapters.LastClientsCursorAdapter;
import com.positiveapps.odkanit.database.tables.TableClient;
import com.positiveapps.odkanit.database.tables.TableLastClients;
import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.main.SearchActivity2;
import com.positiveapps.odkanit.models.ClientOBJ;
import com.positiveapps.odkanit.models.Tik;
import com.positiveapps.odkanit.network.requests.ClientsList;
import com.positiveapps.odkanit.network.requests.TiksList;
import com.positiveapps.odkanit.network.response.ClientItem;
import com.positiveapps.odkanit.network.response.TikItem;
import com.positiveapps.odkanit.objects.onEditTextTypeClient;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Niv on 12/22/2015.
 */
public class SearchClientsFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener, onEditTextTypeClient {



    private ListView listView;
    private ClientCursorAdapter adapter;
    private SearchCustomerFragmentListener listener;
    private ListView listLastClients;
    private LastClientsCursorAdapter lastClientsCursorAdapter;

    public SearchClientsFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        listLastClients.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        showProgressDialog();

        ClientOBJ clientOBJ = MyApp.tableClient.readEntityByID(id);

        ClientOBJ newClient = new ClientOBJ(clientOBJ.getId(),clientOBJ.getName(),clientOBJ.getNumber(),System.currentTimeMillis());
        int j=0;
        ArrayList<ClientOBJ> lastClients = MyApp.tableLastClients.readAll(TableLastClients.TAG_TIME + " ASC");
        for (int i = 0; i <lastClients.size() ; i++) {
            ClientOBJ client= lastClients.get(i);
            if (client.getName().matches(newClient.getName())&&client.getNumber().matches(newClient.getNumber())){

                j++;
            }
        }
        if (j==0) {
            MyApp.tableLastClients.insert(newClient);
        }
        if (lastClients.size()>=10) {
            ClientOBJ firstClient= lastClients.get(0);
            MyApp.tableLastClients.delete(firstClient.getId(),firstClient);
//            System.out.println("firstCustomer "+ firstCustomer.getTimeInMil());
        }

        String name =clientOBJ.getName();
        String number= clientOBJ.getNumber();
        loadTiksFromServer(number, number);
        MyApp.generalSettings.setClientNumber(number);

        MyApp.generalSettings.setTempClient(number);
        MyApp.generalSettings.setTempClientName(name);
        MyApp.generalSettings.setClientWcf(name);
        listener.onNameSelected(name, number);


    }
    private void onLastClientsListClick(AdapterView<?> parent, View view, int position, long id) {
        showProgressDialog();
        ClientOBJ clientOBJ = MyApp.tableLastClients.readEntityByID(id);
        String name =clientOBJ.getName();
        String number= clientOBJ.getNumber();
        MyApp.generalSettings.setClientNumber(number);
        loadTiksFromServer(number, number);
        MyApp.generalSettings.setTempClient(number);
        MyApp.generalSettings.setClientWcf(name);
        MyApp.generalSettings.setTempClientName(name);

        listener.onNameSelected(name, number);

    }

    @Override
    public void onType(String d) {
        if (d.length() >= 2) {
            loadClientsFromServer(d);
            listView.setVisibility(View.VISIBLE);
            listLastClients.setVisibility(View.GONE);
        }else {
            listView.setVisibility(View.GONE);
            listLastClients.setVisibility(View.VISIBLE);
        }

    }


    public interface SearchCustomerFragmentListener {

        void onNameSelected (String s, String num);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // try to set the activity as the listener:
        try {
            listener = (SearchCustomerFragmentListener) activity;
        } catch (ClassCastException e) {
            // the activity does not implement FragmentListListener!
            throw new ClassCastException(activity.toString()
                    + " must implement SearchCustomerFragmentListener!");
        }
    }


    public static SearchClientsFragment newInstance() {
        SearchActivity2.tikSearch = false;
        SearchClientsFragment fragment = new SearchClientsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SearchActivity2.tikSearch = false;
        return inflater.inflate(R.layout.fragment_customer_search, container, false);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (isAdded()) {
            ((SearchActivity2) getBaseActivity()).setTextTypeCallback(this);
        }

        listView = (ListView) view.findViewById(R.id.customerList);
        listLastClients = (ListView)view.findViewById(R.id.lastClients);


        setupClientsListView();
        initClientsLoader();

    }



    private void loadClientsFromServer(String s) {
//        showProgressDialog();
        MyApp.networkManager.initRestAdapter().clientsList(new ClientsList(MyApp.userProfile.getUserId(), s).bodyMap(), new Callback<ArrayList<ClientItem>>() {
            @Override
            public void success(ArrayList<ClientItem> clientsList, Response response) {
//            ToastUtil.toaster(clientsList.toString(), true);
                if (clientsList != null) {
                    addResultsToTable(clientsList);

                }

//            dismisProgressDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                ToastUtil.toaster(error.toString(), true);
//            dismisProgressDialog();
            }
        });


    }

    private void addResultsToTable(ArrayList<ClientItem> clientsList) {

        MyApp.tableClient.deleteAll();

            for (int i = 0; i <clientsList.size() ; i++) {
                ClientOBJ clientOBJ = new ClientOBJ(clientsList.get(i).getName(),clientsList.get(i).getNumber());
                MyApp.tableClient.insert(clientOBJ);
            }
        adapter.notifyDataSetChanged();





    }

    private void initClientsLoader() {
        getLoaderManager().initLoader(0, null, this);
        getLoaderManager().initLoader(1, null, this);

    }


    private void setupClientsListView () {
        adapter = new ClientCursorAdapter(getActivity(),null);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        Cursor cursor = MyApp.tableLastClients.readCursor("", TableLastClients.TAG_TIME + " desc");
        if (cursor.moveToFirst()) {
            lastClientsCursorAdapter = new LastClientsCursorAdapter(getActivity(), cursor);
        }
            listLastClients.setAdapter(lastClientsCursorAdapter);

        listLastClients.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onLastClientsListClick(parent, view, position, id);
            }
        });


    }



    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id){
            case 0:
                return MyApp.tableClient.readLouderCursor(TableClient.TAG_ID+ " ASC");
            case 1:
                return MyApp.tableLastClients.readLouderCursor(TableLastClients.TAG_TIME + " desc");
            default:
                return MyApp.tableClient.readLouderCursor(TableClient.TAG_ID+ " ASC");

        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        adapter.swapCursor(null);


    }


    private void loadTiksFromServer(String clientNum, String searchString) {
        MyApp.networkManager.initRestAdapter().tiksList(
                new TiksList(MyApp.userProfile.getUserId(), clientNum, searchString).bodyMap(), new Callback<ArrayList<TikItem>>() {
                    @Override
                    public void success(ArrayList<TikItem> tiksList, Response response) {
                        if (tiksList != null) {
                            addResultsToTablebag(tiksList);
                            dismisProgressDialog();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ToastUtil.toaster(error.toString(), true);
                        dismisProgressDialog();

                    }
                });


    }

    private void addResultsToTablebag(ArrayList<TikItem> tiksList) {
        MyApp.tableTik.deleteAll();


        for (int i = 0; i < tiksList.size(); i++) {

            Tik tik = new Tik(tiksList.get(i).getName(), tiksList.get(i).getNumber());
            MyApp.tableTik.insertOrUpdate(tik);

        }
        adapter.notifyDataSetChanged();


    }

}
