package com.positiveapps.odkanit.main;

import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.positiveapps.odkanit.fragments.LoginFragment;
import com.positiveapps.odkanit.network.requests.Login;
import com.positiveapps.odkanit.network.response.LoginResult;
import com.positiveapps.odkanit.util.DeviceUtil;
import com.positiveapps.odkanit.util.FragmentsUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

;

/**
 * Created by NIVO on 21/12/2015.
 */
public class LogInActivity extends BaseActivity {




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_login);

        boolean b = MyApp.appPreference.getUserProfil().getLoggedIn();
        if (b){
            performLoginWithSavedUserDetails();


        }else {

            if (savedInstanceState == null) {


                FragmentsUtil.addFragment(getSupportFragmentManager(), LoginFragment.newInstance(), R.id.login_container);

            }

        }

    }

    private void performLoginWithSavedUserDetails() {
        showProgressDialog();
        final String userCode = MyApp.userProfile.getLoggedOfficeCode();
        final String userName = MyApp.userProfile.getLOGED_name();
        final String pass = MyApp.userProfile.getLOGED_pass();
        if (userCode!=null&&userName!=null&&pass!=null) {

            MyApp.networkManager.initRestAdapter().login(new Login(userCode, userName,
                            pass, "android", DeviceUtil.getDeviceUDID()).bodyMap(),
                    new Callback<LoginResult>() {

                        @Override
                        public void success(LoginResult login, Response response) {
                            dismissProgressDialog();

                            //then continue
                            if (login.getApproved()) {
                                MyApp.userProfile.setFirstName(login.getDisplayName());
                                MyApp.userProfile.setUserId(login.getUserID());
                                MyApp.appPreference.getUserProfil().setLoggedIn(true);

                                Intent loginIntent = new Intent(LogInActivity.this, MainActivity.class);
                                startActivity(loginIntent);
                                finish();
                            } else {
                                ToastUtil.toaster(login.getErrorMessage(), true);
                                MyApp.appPreference.getUserProfil().setLoggedIn(false);
                                FragmentsUtil.addFragment(getSupportFragmentManager(), LoginFragment.newInstance(), R.id.login_container);

//                            System.out.println("---> "+login.getErrorMessage());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            dismissProgressDialog();
//
                            FragmentsUtil.addFragment(getSupportFragmentManager(), LoginFragment.newInstance(), R.id.login_container);

                            ToastUtil.toaster(getResources().getString(R.string.login_server_error), true);

                        }
                    });
        }else {
            dismissProgressDialog();

        }

    }
}
