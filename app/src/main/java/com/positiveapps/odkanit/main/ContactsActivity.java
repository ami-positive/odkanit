package com.positiveapps.odkanit.main;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.positiveapps.odkanit.fragments.ContactsFragment;
import com.positiveapps.odkanit.objects.onContactsSelected;
import com.positiveapps.odkanit.util.BackStackUtil;
import com.positiveapps.odkanit.util.FragmentsUtil;
import com.positiveapps.positiveapps.odcanit.R;
import com.quinny898.library.persistentsearch.SearchBox;
import com.quinny898.library.persistentsearch.SearchResult;

public class ContactsActivity extends BaseActivity implements onContactsSelected {



    private SearchBox searchBox;
    private OnSearchListener onSearchListener;
    private boolean closedByBackButton;
    private MenuItem homeMenuButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        searchBox = (SearchBox)findViewById(R.id.searchBox);

        setUpActionBarAndText();

        if (savedInstanceState==null){
            FragmentsUtil.addFragment(getSupportFragmentManager(), ContactsFragment.newInstance(), R.id.contacts_container);
        }



    }



    private void setUpActionBarAndText() {
        setupActionBar(R.id.app_actionbar, getString(R.string.sefer_telefonim));
        getActionBarTextView().setTextColor(Color.WHITE);
//        try {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        } catch (Exception e) {
//            e.printStackTrace();
//            try {
//                getActionBar().setDisplayHomeAsUpEnabled(false);
//            } catch (Exception e1) {
//                e1.printStackTrace();
//
//            }
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        homeMenuButton= menu.findItem(R.id.action_home);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                finish();
                break;
            case R.id.action_search:
//                setupSearchBox();
                onSearchListener.onNewKeyword("");
                return true;
            default:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


//    @Override
//    public boolean dispatchKeyEvent(KeyEvent event) {
//        if(event.getKeyCode() == 4 && searchBox.getSearchOpen()) {
//            closedByBackButton = true;
//            closeSearch();
//            return false;
//        } else {
//            return super.dispatchKeyEvent(event);
//        }
//    }
//
//
//    protected void closeSearch() {
//        searchBox.hideCircularly(this);
//        homeMenuButton.setEnabled(true);
//
//    }






//    private void setupSearchBox() {
//        searchBox.revealFromMenuItem(R.id.action_search, this);
//
//        searchBox.setLogoText(getString(R.string.searcg_in_st));
//
//        searchBox.setMenuVisibility(ViewPager.VISIBLE);
//        try {
//            searchBox.setTextDirection(View.TEXT_DIRECTION_ANY_RTL);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        searchBox.setSearchListener(new SearchBox.SearchListener() {
//
//            @Override
//            public void onSearchOpened() {
//                homeMenuButton.setEnabled(false);
//
//            }
//
//            @Override
//            public void onSearchClosed() {
//                //Use this to un-tint the screen
//                Log.e("searchLog", "on search closed");
//                if (closedByBackButton) {
//                    closedByBackButton = false;
//                } else {
//                    closeSearch();
//                }
//
//                homeMenuButton.setEnabled(true);
//
//
//            }
//
//            @Override
//            public void onSearchTermChanged(String s) {
//                if (onSearchListener != null) {
////                    if (s.length() >= 2) {
//                        onSearchListener.onNewKeyword(s);
////                    }
//                }
//            }
//
//            @Override
//            public void onSearch(String searchTerm) {
//                onSearchListener.onNewKeyword(searchTerm);
//                closeSearch();
//            }
//
//            @Override
//            public void onResultClick(SearchResult result) {
//
//                //React to a result being clicked
//            }
//
//
//            @Override
//            public void onSearchCleared() {
//
//            }
//
//        });
//    }

    public void setOnSearchListener (OnSearchListener onSearchListener){
        this.onSearchListener = onSearchListener;
    }

    @Override
    public void onContactsItemClick() {
//        searchBox.setMenuVisibility(ViewPager.INVISIBLE);

//        onBackPressed();
//        closeSearch();
//
//            if (closedByBackButton) {
//                closedByBackButton = false;
//            } else {
//                closeSearch();
//            }

            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }


    }


    public interface OnSearchListener {
        public void onNewKeyword (String keyword);
    }

}
