package com.positiveapps.odkanit.main;

import android.graphics.Color;
import android.os.Bundle;

import com.positiveapps.odkanit.fragments.ExpensesFragment;
import com.positiveapps.odkanit.util.FragmentsUtil;
import com.positiveapps.positiveapps.odcanit.R;

public class ExpensesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);

        setUpActionBarAndText();

        if (savedInstanceState==null){
            FragmentsUtil.addFragment(getSupportFragmentManager(), ExpensesFragment.newInstance(), R.id.expenses_container);
        }

    }

    private void setUpActionBarAndText() {
        setupActionBar(R.id.app_actionbar, getString(R.string.expenses_reporting));
        getActionBarTextView().setTextColor(Color.WHITE);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                getActionBar().setDisplayHomeAsUpEnabled(false);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

}
