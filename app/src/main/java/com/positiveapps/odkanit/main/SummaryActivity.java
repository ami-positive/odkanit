package com.positiveapps.odkanit.main;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.positiveapps.odkanit.adapters.ReportsAdapter;
import com.positiveapps.odkanit.models.Report;
import com.positiveapps.odkanit.network.requests.Login;
import com.positiveapps.odkanit.network.requests.ReportRequest;
import com.positiveapps.odkanit.network.response.LoginResult;
import com.positiveapps.odkanit.network.response.ReportItem;
import com.positiveapps.odkanit.util.DateUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import roboguice.inject.InjectView;

public class SummaryActivity extends BaseActivity implements View.OnClickListener {

    private TextView dayTV;
    private TextView weekTV;
    private TextView monthTV;
    private ListView sumList;
    private TextView houersTV;
    private ArrayList<ReportItem> reportsArray;
    private ReportsAdapter sumAdapter;
    private ArrayList<Report> r;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        setUpActionBarAndText();

        findViews();
        setListeners();
        setDayTVselected();

        setUpAdapter();
//        r = new ArrayList<>();
        /// for now
//        r.add(new Report("03/03/2016", "case 1", "10:22"));
//        r.add(new Report("03/03/2016", "case 2", "11:22"));
//        r.add(new Report("02/03/2016", "case 3", "10:22"));
//        r.add(new Report("04/03/2016", "case 4", "2:12"));
//        r.add(new Report("01/03/2016", "case 5", "1:22"));
//        r.add(new Report("28/02/2016", "case 6", "03:24"));
//        r.add(new Report("29/02/2016", "case 7", "01:22"));
//        r.add(new Report("04/03/2016", "case 8", "01:34"));
//        r.add(new Report("03/03/2016", "case 1", "10:22"));
//        r.add(new Report("03/03/2016", "case 2", "11:22"));
//        r.add(new Report("02/03/2016", "case 3", "10:22"));
//        r.add(new Report("04/03/2016", "case 4", "2:12"));
//        r.add(new Report("01/03/2016", "case 5", "1:22"));
//        r.add(new Report("28/02/2016", "case 6", "03:24"));
//        r.add(new Report("29/02/2016", "case 7", "01:22"));
//        r.add(new Report("04/03/2016", "case 8", "01:34"));
//        r.add(new Report("03/03/2016", "case 1", "10:22"));
//        r.add(new Report("03/03/2016", "case 2", "11:22"));
//        r.add(new Report("02/03/2016", "case 3", "10:22"));
//        r.add(new Report("04/03/2016", "case 4", "2:12"));
//        r.add(new Report("01/03/2016", "case 5", "1:22"));
//        r.add(new Report("28/02/2016", "case 6", "03:24"));
//        r.add(new Report("29/02/2016", "case 7", "01:22"));
//        r.add(new Report("05/03/2016", "case 8", "01:34"));
//        r.add(new Report("05/03/2016", "case 2", "11:22"));
//        r.add(new Report("05/03/2016", "case 3", "10:22"));
//        r.add(new Report("05/03/2016", "case 2", "11:22"));
//        r.add(new Report("05/03/2016", "case 3", "10:22"));
//        r.add(new Report("06/03/2016", "case 8", "01:34"));
//        r.add(new Report("06/03/2016", "case 2", "11:22"));
//        r.add(new Report("06/03/2016", "case 3", "10:22"));
//        r.add(new Report("06/03/2016", "case 2", "11:22"));
//        r.add(new Report("06/03/2016", "case 3", "10:22"));
//        r.add(new Report("05/03/2016", "case 2", "11:22"));
//        r.add(new Report("05/03/2016", "case 3", "10:22"));
//        r.add(new Report("06/02/2016", "case x", "01:34"));
//        r.add(new Report("06/02/2016", "case x", "11:22"));
//        r.add(new Report("06/02/2016", "case x", "10:22"));
//        r.add(new Report("06/02/2016", "case x", "11:22"));
//        r.add(new Report("06/02/2016", "case x", "10:22"));
//        r.add(new Report("05/03/2016", "case 3", "10:22"));
//        r.add(new Report("27/02/2016", "case y", "01:34"));
//        r.add(new Report("27/02/2016", "case y", "11:22"));
//        r.add(new Report("27/02/2016", "case y", "10:22"));
//        r.add(new Report("27/02/2016", "case y", "11:22"));
//        r.add(new Report("27/02/2016", "case y", "10:22"));
//        r.add(new Report("06/02/2016", "case x", "01:34"));
//        r.add(new Report("06/02/2016", "case x", "11:22"));
//        r.add(new Report("06/02/2016", "case x", "10:22"));
//        r.add(new Report("06/02/2016", "case x", "11:22"));
//        r.add(new Report("06/02/2016", "case x", "10:22"));
//        r.add(new Report("05/03/2016", "case 3", "10:22"));
//        r.add(new Report("27/02/2016", "case y", "01:34"));
//        r.add(new Report("08/03/2016", "case x", "01:34"));
//        r.add(new Report("08/03/2016", "case x", "11:22"));
//        r.add(new Report("09/03/2016", "case x", "10:22"));
//        r.add(new Report("10/03/2016", "case x", "11:22"));
//        r.add(new Report("07/03/2016", "case x", "10:22"));
//        r.add(new Report("07/03/2016", "case 3", "10:22"));
//        r.add(new Report("27/03/2016", "case y", "01:34"));
//        r.add(new Report("08/03/2016", "case x", "11:22"));
//        r.add(new Report("09/03/2016", "case x", "10:22"));
//        r.add(new Report("10/03/2016", "case x", "11:22"));
//        r.add(new Report("07/03/2016", "case x", "10:22"));
//        r.add(new Report("07/03/2016", "case 3", "10:22"));
//        r.add(new Report("11/03/2016", "case y", "01:34"));
//        r.add(new Report("07/03/2016", "case x", "10:22"));
//        r.add(new Report("07/03/2016", "case 3", "10:22"));
//        r.add(new Report("11/03/2016", "case y", "01:34"));
        sumAdapter.notifyDataSetChanged();

//        System.out.println("niv " + r.toString());
//        Collections.sort(r);


//        Collections.sort(r, new Comparator<Report>() {
//            @Override
//            public int compare(Report lhs, Report rhs) {
//                return lhs.getReportDate() .compareTo( rhs.getReportDate());
//            }
//        });




    }

    @Override
    protected void onResume() {
        super.onResume();

        getReportsRequest(0);
        setDayTVselected();
//        sortByToday();


    }

    private void getReportsRequest(int daysBack){
        int user= MyApp.userProfile.getUserId();
        showProgressDialog();
        MyApp.networkManager.initRestAdapter().getReport(new ReportRequest(user, daysBack).bodyMap(), new Callback<ArrayList<ReportItem>>() {

            @Override
            public void success(ArrayList<ReportItem> reportItems, Response response) {
                if (reportItems != null) {
                    sumAdapter.clear();
                    for (ReportItem r : reportItems) {
                        reportsArray.add(r);
                    }
                    sumAdapter.notifyDataSetChanged();
                    Collections.sort(reportsArray);
                    clcSums(reportItems);
                }
                dismissProgressDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                ToastUtil.toaster(error.toString(), true);
                dismissProgressDialog();
            }
        });

    }

    private void clcSums(ArrayList<ReportItem> reportItems) {

        double sum=0;

        for (ReportItem reportItem: reportItems){
            if (reportItem.getType().equals("Billing")) {
                double time = reportItem.getAmount();
                sum += time;
            }
        }
        long mins =  Math.round((sum%1)*60);
        long hours = (long) (sum/1);
        String newSum = hours+"."+mins;
        System.out.println("split "+hours+" ="+mins);
        newSum= getShortDistance(newSum);
//        try {
//            newSum= newSum.replace('.',':');
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        String[] splited = newSum.split(Pattern.quote("."));
        if (splited.length>0) {
            newSum  = splited[0]+":"+(splited[1].length()>1?splited[1]:splited[1]+"0");
        }

//        boolean succes=true;
//        long mins=0;
//        System.out.println("niv sum "+sum);
//        String minutes= sum+"";
//        minutes=getShortDistance(minutes);
//        String[] splited = minutes.split(Pattern.quote("."));
//        if (splited.length>0) {
//
//            System.out.println("niv spli "+splited[1]);
//            mins = Long.parseLong(splited[1]);
//            if (mins<10)
//                mins=mins*10;
//            //7-->>>70
////            float x =((int) (mins * 100)) / 100f;
//        }
//        else
//            succes=false;
//
//        mins= mins*60/100;
//        //70--->42
//
//
////        System.out.print(" niv ="+ report.getAmount()+"|"+(report.getAmount()%1)+"|");
////        int hours = (int) (report.getAmount());
//        String bilTime = splited[0]+":"+(mins>9?mins:("0"+mins));

//        if (succes) {
//            houersTV.setText(bilTime);
//        }
//        else
            houersTV.setText(newSum);


//        DateTime sumTime= new DateTime(0);
//        long time = 0;
//        for (ReportItem reportItem: reportItems){
//            if (reportItem.getType().equals("Billing")){
//                DateTime dateTime =new DateTime(reportItem.getDate());
//                int hour= dateTime.getHourOfDay();
//                int min = dateTime.getMinuteOfHour();
//                sumTime.plusHours(hour);
//                sumTime.plusMinutes(min);
//                time+=hour*60*60000;
//                time+=min*60000;
//
//            }
////            else if (reportItem.getType().equals("Expense")){
////
////            }
//        }
////        String hourSum= DateUtil.getTimeAsStringByMilli(DateUtil.FORMAT_JUST_TIME,time);
//        int hours = (int)time/60000/60;
//        int min = (int)time/60000%60;
//        String minString= min<10? "0"+min: min+"";

    }
    private String getShortDistance(String distance) {
        double dis = 0;
        if (distance != "")
            try {
                dis = Double.parseDouble(distance);
                String s = ((int) (dis * 100)) / 100f + "";

                return s;
            } catch (Exception e) {
                e.printStackTrace();
            }

        return "";

    }
    private void setUpAdapter() {
        reportsArray = new ArrayList<>();
        sumAdapter = new ReportsAdapter(this, reportsArray);
        sumList.setAdapter(sumAdapter);
    }

    private void findViews() {
        dayTV=(TextView)findViewById(R.id.dayTV);
        weekTV=(TextView)findViewById(R.id.weekTV);
        monthTV=(TextView)findViewById(R.id.monthTV);

        sumList=(ListView)findViewById(R.id.sumList);
        houersTV=(TextView)findViewById(R.id.houersTV);
    }
    private void setListeners() {
        dayTV.setOnClickListener(this);
        weekTV.setOnClickListener(this);
        monthTV.setOnClickListener(this);


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dayTV:
                setDayTVselected();
//                sortByToday();
                getReportsRequest(0);

                break;
            case R.id.weekTV:
                setWeekTvSelected();
//                sortByThisWeek();
                getReportsRequest(7);

                break;
            case R.id.monthTV:
                SetMonthTvSelected();
                getReportsRequest(30);

//                sortByMonth();


        break;
        }

    }

//    private void sortByMonth() {
//        sumAdapter.clear();
//        String now = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE,System.currentTimeMillis());
//        DateTime nowDate= new DateTime(System.currentTimeMillis());
//        for (Report report :r ) {
//            long date1 = DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE, report.getReportDate());
//            DateTime reportDate = new DateTime(date1);
//            if (reportDate.getMonthOfYear()==nowDate.getMonthOfYear()
//                    && reportDate.getYear()==nowDate.getYear()){
//                reportsArray.add(report);
//            }
//
//
//        }
//        sumAdapter.notifyDataSetChanged();
//
//    }

//    private void sortByThisWeek() {
//
//        sumAdapter.clear();
//        DateTime nowDate= new DateTime(System.currentTimeMillis());
//
//        int nowDayOfWeek= nowDate.getDayOfWeek()+1;
//        if (nowDayOfWeek==8)
//            nowDayOfWeek=1;
//        for (Report report :r ){
//            long date1 =DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE, report.getReportDate());
//            DateTime reportDate = new DateTime(date1);
//            int reportDayOfWeek= reportDate.getDayOfWeek()+1;
//
////            System.out.println("niv - nowdate: "+nowDate.toString()+" reportDate: "+reportDate.toString());
//            boolean reportIsInTheSameWeek= getReportIsInTheSameWeek(nowDate,reportDate,nowDayOfWeek);
//
//            if (reportIsInTheSameWeek) {
//                reportsArray.add(report);
//            }
//
//        }
//        sumAdapter.notifyDataSetChanged();
//
//    }

//    private boolean getReportIsInTheSameWeek(DateTime nowDate, DateTime reportDate,
//                                             int nowDayOfWeek) {
//        ArrayList<DateTime> weekDates = new ArrayList<>();
//        for (int i=nowDayOfWeek;i>=1;i--){
//            weekDates.add(nowDate.minusDays(i - 1));
//        }
//        System.out.println("weekDates: "+weekDates.toString());
//
//        for (int j=0;j<weekDates.size();j++){
//            if (weekDates.get(j).getMonthOfYear()==reportDate.getMonthOfYear()
//                    &&weekDates.get(j).getYear()==reportDate.getYear()
//                    &&weekDates.get(j).getDayOfMonth() == reportDate.getDayOfMonth())
//            {
//                return true;
//            }
//        }
//
//        return false;
//    }

//    private void sortByToday() {
//        sumAdapter.clear();
//        String now = DateUtil.getDateAsStringByMilli(DateUtil.FORMAT_JUST_DATE, System.currentTimeMillis());
//        DateTime nowDate= new DateTime(System.currentTimeMillis());
//        for (Report report :r ){
//            long date1 =DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE, report.getReportDate());
//            DateTime reportDate = new DateTime(date1);
//
//            System.out.println("niv - nowdate: "+nowDate.toString()+" reportDate: "+reportDate.toString());
//            if (reportDate.getMonthOfYear()==nowDate.getMonthOfYear()
//                    && reportDate.getYear()==nowDate.getYear()
//                    && reportDate.getDayOfMonth() == nowDate.getDayOfMonth())
//            {
//                reportsArray.add(report);
//            }
//
//        }
//        sumAdapter.notifyDataSetChanged();
//
//    }

    private void SetMonthTvSelected() {
        dayTV.setTextColor(getResources().getColor(R.color.od_dark_blue));
        dayTV.setBackgroundColor(getResources().getColor(R.color.WHITE));
        weekTV.setTextColor(getResources().getColor(R.color.od_dark_blue));
        weekTV.setBackgroundColor(getResources().getColor(R.color.WHITE));
        monthTV.setTextColor(getResources().getColor(R.color.WHITE));
        monthTV.setBackgroundColor(getResources().getColor(R.color.od_yellow));
    }

    private void setWeekTvSelected() {
        dayTV.setTextColor(getResources().getColor(R.color.od_dark_blue));
        dayTV.setBackgroundColor(getResources().getColor(R.color.WHITE));
        weekTV.setTextColor(getResources().getColor(R.color.WHITE));
        weekTV.setBackgroundColor(getResources().getColor(R.color.od_yellow));
        monthTV.setTextColor(getResources().getColor(R.color.od_dark_blue));
        monthTV.setBackgroundColor(getResources().getColor(R.color.WHITE));
    }

    private void setDayTVselected(){
        dayTV.setTextColor(getResources().getColor(R.color.WHITE));
        dayTV.setBackgroundColor(getResources().getColor(R.color.od_yellow));

        weekTV.setTextColor(getResources().getColor(R.color.od_dark_blue));
        weekTV.setBackgroundColor(getResources().getColor(R.color.WHITE));
        monthTV.setTextColor(getResources().getColor(R.color.od_dark_blue));
        monthTV.setBackgroundColor(getResources().getColor(R.color.WHITE));
    }


    private void setUpActionBarAndText() {
        setupActionBar(R.id.app_actionbar, getString(R.string.od_doh_mesakem));
        getActionBarTextView().setTextColor(Color.WHITE);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                getActionBar().setDisplayHomeAsUpEnabled(false);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }


}
