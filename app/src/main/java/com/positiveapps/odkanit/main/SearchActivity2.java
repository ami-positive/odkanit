package com.positiveapps.odkanit.main;


import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.positiveapps.odkanit.adapters.MyPagerAdapter;
import com.positiveapps.odkanit.fragments.SearchBagFragment;
import com.positiveapps.odkanit.fragments.SearchClientsFragment;
import com.positiveapps.odkanit.objects.onEditTextTypeClient;
import com.positiveapps.odkanit.objects.onEditTextTypeTiks;
import com.positiveapps.odkanit.ui.PagerSlidingTabStrip;
import com.positiveapps.odkanit.util.AppUtil;
import com.positiveapps.positiveapps.odcanit.R;

import java.util.Objects;

public class SearchActivity2 extends BaseActivity implements SearchClientsFragment.SearchCustomerFragmentListener, SearchBagFragment.SearchBagFragmentListener {


    private static final String TAG = "SearchActivity";

    public static final String CLIENT_NUMBER = "clientNumber";
    public static final String CLIENT_NAME = "clientName";
    public static final String BAG_NAME = "bagName";
    public static final String BAG_NUMBER = "bagNumber";



    private ViewPager viewPager;
    private MyPagerAdapter pagerAdapter;
    public static String ACTION_BAG = "ACTION_BAG";
    public onEditTextTypeTiks callback;
    public onEditTextTypeClient callback2;
    private String name;
    private String clientNum;
    private boolean b;
    public static boolean tikSearch;
    private Button clearB;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search2);

        setUpActionBarAndText();


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        // fragment manger for switching fragments
        FragmentManager fm = getSupportFragmentManager();

        // arainging adapter:
        pagerAdapter = new MyPagerAdapter(fm, this);

        viewPager.setAdapter(pagerAdapter);

        tabStrip.setViewPager(viewPager);




        Intent i = getIntent();
        System.out.println("i");
        if (i.getAction()!=null) {
            b=i.getAction().matches(ACTION_BAG);
        }

        if (!b) {
            viewPager.setCurrentItem(1, true);
        }





        final EditText editText= (EditText)findViewById(R.id.editTextSearch);

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    int v1= viewPager.getCurrentItem();
                    if (v1==0) {
                        callback.onType(editText.getText().toString());
                    } else {
                        callback2.onType(editText.getText().toString());
                    }
                    AppUtil.hideKeyBoard(editText);

                    return true;
                }
                return false;
            }
        });


//        if (viewPager.getCurrentItem()==0){
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, final int count) {
                    int v= viewPager.getCurrentItem();
                    if (v==0){
                    if (count<=1)
                        if (!MyApp.generalSettings.getClientNumber().equals("")) {
                            callback.onType(MyApp.generalSettings.getClientNumber());
                            System.out.println("getClientNumber =" + MyApp.generalSettings.getClientNumber());

                        }
                    }
                    if(count > 1) {


                        System.out.println("tikSearch " +v );
                        if (v==0) {

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    if (editText.getText().toString().length() == count)
                                        callback.onType(editText.getText().toString());

                                }
                            }, 500);


                        } else {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    if (editText.getText().toString().length() == count)
                                        callback2.onType(editText.getText().toString());

                                }
                            }, 500);

                        }
                    }else if (v==1){
                        callback2.onType(s.toString());
                    }

                    if (count>=1){
                        clearB.setVisibility(View.VISIBLE);
                    }else {
                        clearB.setVisibility(View.INVISIBLE);

                    }

                }

                @Override
                public void afterTextChanged(Editable s) {


                }
            });


        clearB=(Button)findViewById(R.id.clearB);

        clearB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });


    }

    private void setUpActionBarAndText() {
        setupActionBar(R.id.app_actionbar, getString(R.string.search));
        getActionBarTextView().setTextColor(Color.WHITE);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                getActionBar().setDisplayHomeAsUpEnabled(false);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }


    public void setTextTypeCallback(onEditTextTypeTiks callback) {
        this.callback = callback;
    }

    public void setTextTypeCallback(onEditTextTypeClient callback) {
        this.callback2 = callback;
    }

    @Override
    public void onNameSelected(String s,String num) {


        name= s;
        clientNum= num;
        viewPager.setCurrentItem(0, true);
        EditText editText1= (EditText)findViewById(R.id.editTextSearch);
        editText1.setText("");

//        Intent res= new Intent();
//        res.putExtra("text",s);
//        setResult(0, res);
//        finish();
    }

    @Override
    public void onBagSelected(String bagName,String num) {


        Intent i = getIntent();
        System.out.println("i");
        if (i.getAction()!=null) {
            b =i.getAction().equals(ACTION_BAG);
        }
        Intent res= new Intent();



        if (b){
            res.putExtra(BAG_NAME, bagName);
            res.putExtra(BAG_NUMBER, num);
            setResult(1, res);


        }else {

            res.putExtra(CLIENT_NAME,name);
            res.putExtra(CLIENT_NUMBER,clientNum);
            res.putExtra(BAG_NAME, bagName);
            res.putExtra(BAG_NUMBER, num);
            setResult(0, res);
        }

        finish();
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_search_activity, menu);
//        return true;
//
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//                case R.id.action_search2:
//                setupSearchBox();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    private void setupSearchBox() {
//
//    }

}
