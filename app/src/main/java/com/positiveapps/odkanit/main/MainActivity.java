package com.positiveapps.odkanit.main;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.positiveapps.odkanit.fragments.MainFragment;
import com.positiveapps.odkanit.util.AppUtil;
import com.positiveapps.odkanit.util.ContentProviderUtil;
import com.positiveapps.odkanit.util.FragmentsUtil;
import com.positiveapps.odkanit.util.Helper;
import com.positiveapps.odkanit.util.PermissionUtil;
import com.positiveapps.odkanit.util.ToastUtil;
import com.positiveapps.positiveapps.odcanit.R;


public class MainActivity extends BaseActivity {


    private static final int REQUEST_LOCATION = 1;
    public static DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private static String[] PERMISSIONS_LOC = {
            Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupActionBar(R.id.app_actionbar, getString(R.string.odkanit_name));
        getActionBarTextView().setTextColor(Color.WHITE);

        setUpDrawerNavigation();

        if (savedInstanceState == null) {
            FragmentsUtil.addFragment(getSupportFragmentManager(), MainFragment.newInstance(), R.id.main_container);

        }

        setHamburgerListener();

        if (Build.VERSION.SDK_INT > 22) {
            checkLocationPermisions();
        }else {
            AppUtil.startLocationService(this);
        }



    }

    private void checkLocationPermisions() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestLocationPermission();

        }else {
            AppUtil.startLocationService(this);

        }

    }

    private void requestLocationPermission() {
        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)&&
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                ) {

            ActivityCompat.requestPermissions(MainActivity.this,
                    PERMISSIONS_LOC,
                    REQUEST_LOCATION);
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
//            final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
//            alert.setTitle(this.getResources().getString(R.string.location_per));
////            alert.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
////                @Override
////                public void onClick(DialogInterface dialog, int id) {
////
////                    dialog.dismiss();
////                }
////            });
//            alert.setPositiveButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    ActivityCompat.requestPermissions(MainActivity.this,
//                            PERMISSIONS_LOC,
//                            REQUEST_LOCATION);
//                    dialog.dismiss();
//                }
//            });
//            alert.show();

        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_LOC,
                    REQUEST_LOCATION);
        }
        // END_INCLUDE(camera_permission_request)
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        System.out.println("onRequestPermissionsResult");

        if (requestCode == REQUEST_LOCATION) {
            System.out.println("onRequestPermissionsResult l location");

            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            // Check if the only required permission has been granted
            if (PermissionUtil.verifyPermissions(grantResults)){
                System.out.println("onRequestPermissionsResult granted");

                AppUtil.startLocationService(this);
            }
//            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                System.out.println("niv onRequestPermissionsResult granted");
//
//                AppUtil.startLocationService(this);
//
//            }
            // END_INCLUDE(permission_result)

        }

    }

    /* Note: Methods and definitions below are only used to provide the UI for this sample and are
    not relevant for the execution of the runtime permissions API. */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

        }
        return super.onOptionsItemSelected(item);

    }

    private void setHamburgerListener() {
        getActionbarToolbar().setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ToastUtil.toaster("hamburger", true);

                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });
    }

    private void setUpDrawerNavigation() {
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e1) {
            e1.printStackTrace();

        }
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        mDrawerToggle = new ActionBarDrawerToggle
                (this, mDrawerLayout, getActionbarToolbar(), 0, 0) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };


        //Setting the actionbarToggle to drawer layout
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();



    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppUtil.stopLocationService(this);

    }

    @Override
        public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)){
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }else {

            if (currentFragment instanceof MainFragment) {
                if (MyApp.userSettings.getTimerState() == 1) {


                    LayoutInflater factory = LayoutInflater.from(this);
                    final View deleteDialogView = factory.inflate(
                            R.layout.choose_exit_dialog, null);
                    final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
                    deleteDialog.setView(deleteDialogView);
                    deleteDialogView.findViewById(R.id.okBuutonDialog).setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            //your business logic
                            finish();
                            deleteDialog.dismiss();
                        }

                    });
                    deleteDialogView.findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            deleteDialog.dismiss();

                        }
                    });

                    deleteDialog.show();

                } else {
                    super.onBackPressed();
                }
            } else {
                super.onBackPressed();
            }
        }
    }
}
