package com.positiveapps.odkanit.main;

import android.graphics.Color;
import android.os.Bundle;

import com.positiveapps.odkanit.fragments.PresenceReportingFragment;
import com.positiveapps.odkanit.util.FragmentsUtil;
import com.positiveapps.positiveapps.odcanit.R;

public class PresenceReportingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presence_reporting);

        setUpActionBarAndText();


        if (savedInstanceState==null){
            FragmentsUtil.addFragment(getSupportFragmentManager(), PresenceReportingFragment.newInstance(), R.id.presence_reporting_container);
        }


    }
    private void setUpActionBarAndText() {
        setupActionBar(R.id.app_actionbar, getString(R.string.presence_reporting));
        getActionBarTextView().setTextColor(Color.WHITE);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                getActionBar().setDisplayHomeAsUpEnabled(false);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

}
