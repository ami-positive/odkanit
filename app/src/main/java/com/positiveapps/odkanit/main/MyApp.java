package com.positiveapps.odkanit.main;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.positiveapps.odkanit.database.tables.Table10Contacts;
import com.positiveapps.odkanit.database.tables.TableBilling;
import com.positiveapps.odkanit.database.tables.TableClient;
import com.positiveapps.odkanit.database.tables.TableCustomer;
import com.positiveapps.odkanit.database.tables.TableLastClients;
import com.positiveapps.odkanit.database.tables.TableObserver;
import com.positiveapps.odkanit.database.tables.TableTik;
import com.positiveapps.odkanit.network.NetworkManager;
import com.positiveapps.odkanit.objects.TimeCounter;
import com.positiveapps.odkanit.storage.AppPreference;
import com.positiveapps.odkanit.storage.StorageManager;
import com.positiveapps.odkanit.util.DeviceUtil;

import java.util.ArrayList;

/**
 * Created by natiapplications on 16/08/15.
 */
public class MyApp extends Application {



    public static BaseActivity mainInstance;


    public static Context appContext;
    public static String appVersion;
    public static TimeCounter timeCount;



    public static AppPreference appPreference;
    public static AppPreference.PreferenceUserSettings userSettings;
    public static AppPreference.PreferenceGeneralSettings generalSettings;
    public static AppPreference.PreferenceUserProfile userProfile;


    public static BaseActivity currentActivity;


    //managers
    public static NetworkManager networkManager;
    public static StorageManager storageManager;

    public static TableCustomer tableCustomer;
    public static TableBilling tableBilling;
    public static TableClient tableClient;
    public static TableTik tableTik;
    public static Table10Contacts table10Contacts;
    public static TableLastClients tableLastClients;



    public static ArrayList<String> currentPlaceIds = new ArrayList<String>();

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        appContext = getApplicationContext();
        appVersion = DeviceUtil.getDeviceVersion();
        loadDataBase();
        loadPreferences();
        networkManager = NetworkManager.getInstance(appContext);
        storageManager = StorageManager.getInstance();
        timeCount = TimeCounter.getInstance();
    }




    private void loadPreferences() {
        appPreference = AppPreference.getInstans(appContext);
        userSettings = appPreference.getUserSettings();
        userProfile = appPreference.getUserProfil();
        generalSettings = appPreference.getGeneralSettings();
    }


    public static void addTablesObservers (TableObserver observer){
       // MyApp.tableManager.addObserver(observer);

    }

    public static void removeTablesObservers (TableObserver observer){
       // MyApp.tableManager.removeObserver(observer);

    }

    public static void loadDataBase (){
        tableCustomer = TableCustomer.getInstance(appContext);
        tableBilling = TableBilling.getInstance(appContext);
        tableClient = TableClient.getInstance(appContext);
        tableTik = TableTik.getInstance(appContext);
        table10Contacts = Table10Contacts.getInstance(appContext);
        tableLastClients = TableLastClients.getInstance(appContext);
    }

    public static void closeDatabase () {
        MyApp.tableCustomer.close();
        MyApp.tableBilling.close();
        MyApp.tableClient.close();
        MyApp.tableTik.close();
        MyApp.table10Contacts.close();
        MyApp.tableLastClients.close();

    }

    public static void deleteDatabase () {
        MyApp.tableCustomer.deleteAll();
        MyApp.tableBilling.deleteAll();
        MyApp.tableClient.deleteAll();
        MyApp.tableTik.deleteAll();
        MyApp.table10Contacts.deleteAll();
        MyApp.tableLastClients.deleteAll();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
