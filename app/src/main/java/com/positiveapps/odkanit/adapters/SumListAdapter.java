//package com.positiveapps.odkanit.adapters;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.TextView;
//
//import com.positiveapps.odkanit.models.Report;
//import com.positiveapps.positiveapps.odcanit.R;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Niv on 3/3/2016.
// */
//public class SumListAdapter extends ArrayAdapter<Report>{
//
//        public SumListAdapter(Context context, ArrayList<Report> reports) {
//            super(context, 0, reports);
//        }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        Report report = getItem(position);
//        // Check if an existing view is being reused, otherwise inflate the view
//        if (convertView == null) {
//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.report_list_item, parent, false);
//        }
//
//        TextView reportTimeListItem = (TextView) convertView.findViewById(R.id.reportTimeListItem);
//        TextView bagNameListItem = (TextView) convertView.findViewById(R.id.bagNameListItem);
//        TextView dateListItem = (TextView) convertView.findViewById(R.id.dateListItem);
//
//        String time= report.getReportTime();
//        String bag= report.getReportBag();
//        String date= report.getReportDate();
//
//        reportTimeListItem.setText(time);
//        bagNameListItem.setText(bag);
//        dateListItem.setText(date);
//
//
//
//        return convertView;
//
//    }
//}
