package com.positiveapps.odkanit.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.odkanit.models.Report;
import com.positiveapps.odkanit.network.response.ReportItem;
import com.positiveapps.positiveapps.odcanit.R;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by Niv on 3/10/2016.
 */
public class ReportsAdapter extends ArrayAdapter<ReportItem> {
    Context c ;

    public ReportsAdapter(Context context, ArrayList<ReportItem> reports) {
        super(context, 0, reports);
        c=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ReportItem report = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.report_list_item, parent, false);
        }

        TextView reportTimeListItem = (TextView) convertView.findViewById(R.id.reportTimeListItem);
        TextView bagNameListItem = (TextView) convertView.findViewById(R.id.bagNameListItem);
        TextView dateListItem = (TextView) convertView.findViewById(R.id.dateListItem);
        ImageView sumImage = (ImageView) convertView.findViewById(R.id.sumImage);
        DateTime dateTime= new DateTime(report.getDate());

//        String minString= dateTime.getMinuteOfHour()<10? "0"+dateTime.getMinuteOfHour(): dateTime.getMinuteOfHour()+"";
//
//        String time= dateTime.getHourOfDay()+":"+minString;
//        String time2= report.
        String bag= report.getTikName();
        String date= dateTime.getDayOfMonth()+"/"+dateTime.getMonthOfYear()+"/"+dateTime.getYear();

        String amount = report.getAmount()+"";
        String type =report.getType();

//        reportTimeListItem.setText(time);

        dateListItem.setText(date);
        int mins = (int)  Math.round(((report.getAmount()%1)*60));
        int hours = (int) (report.getAmount()/1);

//        int mins = (int) ((report.getAmount()%1)*60/100);
//        long mins=0;
//        String minutes= report.getAmount()+"";
//        String[] splited = minutes.split(Pattern.quote("."));
//        if (splited.length>0) {
//            mins = Long.parseLong(splited[1]);
//            //7
//        }
//        mins= mins*60/100;


//        System.out.print(" niv ="+ report.getAmount()+"|"+(report.getAmount()%1)+"|");
//        int hours = (int) (report.getAmount());
//        String bilTime = splited[0]+":"+(mins>9?mins:("0"+mins));
        String bilTime = hours+":"+(mins>9?mins:("0"+mins));



        if (type.equals("Billing")){
            reportTimeListItem.setText(bilTime);
            reportTimeListItem.setTextColor(convertView.getResources().getColor(R.color.od_dark_orange));
            sumImage.setImageResource(R.mipmap.hours_icon);
            sumImage.setVisibility(View.VISIBLE);
            bagNameListItem.setText(bag);
        }
        if (type.equals("Expense")){


            String[] splited = amount.split(Pattern.quote("."));
            if (splited.length>0) {
                if (splited[1].length() < 2) {
                    amount=amount+"0";
                }
                if (splited[1].equals("0")||splited[1].equals("00")){
                    amount=splited[0];
                }

            }else {
                amount = (int)report.getAmount()+"";
            }


            reportTimeListItem.setText(amount);
            reportTimeListItem.setTextColor(convertView.getResources().getColor(R.color.green_2));
            sumImage.setImageResource(R.mipmap.sum_icon);
            sumImage.setVisibility(View.VISIBLE);
            bagNameListItem.setText(bag);

        }if (type.equals("Presence")){
            reportTimeListItem.setText(bilTime);
            reportTimeListItem.setTextColor(convertView.getResources().getColor(R.color.od_dark_blue));
            sumImage.setVisibility(View.INVISIBLE);
            bagNameListItem.setText(report.getDescription());
        }
        if (type.equals("Absense")){
            reportTimeListItem.setText(bilTime);
            reportTimeListItem.setTextColor(convertView.getResources().getColor(R.color.od_dark_red));
            sumImage.setVisibility(View.INVISIBLE);
            bagNameListItem.setText(report.getDescription());

        }

        return convertView;

    }
}
