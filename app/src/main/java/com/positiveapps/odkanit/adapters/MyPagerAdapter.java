package com.positiveapps.odkanit.adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;

import com.positiveapps.odkanit.fragments.SearchBagFragment;
import com.positiveapps.odkanit.fragments.SearchClientsFragment;
import com.positiveapps.odkanit.ui.PagerSlidingTabStrip;
import com.positiveapps.positiveapps.odcanit.R;


public class MyPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.ViewTabProvider{

    private View[] TABS;

    public MyPagerAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        TABS = new View[2];

        TABS[0] = LayoutInflater.from(activity).inflate(R.layout.search_bag_tab,null);
        TABS[1] = LayoutInflater.from(activity).inflate(R.layout.search_client_tab, null);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:

                return SearchBagFragment.newInstance();

            case 1:

                return SearchClientsFragment.newInstance();




            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 1:
                return "חיפוש לקוח";
            case 0:
                return "חיפוש תיק";


            default:
                return null;

        }
    }

    public  void setTabs(View[] tabs) {
        TABS = tabs;
    }

    @Override
    public View getPageView(int position) {
        return TABS[position];
    }
}
