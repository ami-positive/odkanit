package com.positiveapps.odkanit.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.models.ClientOBJ;
import com.positiveapps.positiveapps.odcanit.R;


public class ClientCursorAdapter extends CursorAdapter {
    //create holder
    class ViewHolder {
        //insert attributes to holder
        long id;
        public TextView textView;
        public ImageView imageView;
    }


    public ClientCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.customer_list_item, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.textView = (TextView)view.findViewById(R.id.textViewCustomer_item);

        holder.imageView=(ImageView)view.findViewById(R.id.imageViewCustomerList);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ClientOBJ client = MyApp.tableClient.cursorToEntity(cursor);
        ViewHolder holder = (ViewHolder) view.getTag();

        String name = client.getName()+"("+client.getNumber()+")";
        holder.textView.setText(name);



    }
}
