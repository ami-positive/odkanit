package com.positiveapps.odkanit.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.odkanit.main.MyApp;
import com.positiveapps.odkanit.models.Customer;
import com.positiveapps.positiveapps.odcanit.R;


public class ContactsCursorAdapter extends CursorAdapter {
    //create holder
    class ViewHolder {
        //insert attributes to holder
        long id;
        public TextView textView;
        public ImageView imageView;
    }


    public ContactsCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.contacts_list_item, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.textView = (TextView)view.findViewById(R.id.textContact);

        holder.imageView=(ImageView)view.findViewById(R.id.imageViewType);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {


        Customer customer = MyApp.tableCustomer.cursorToEntity(cursor);
        ViewHolder holder = (ViewHolder) view.getTag();
        String name = customer.getName();

        holder.textView.setText(name);

        String typeImage = customer.getType();
        if (typeImage.equals("לקוח"))
            holder.imageView.setImageResource(R.mipmap.customer_blue_icon);
            else if (typeImage.equals("ספק"))
            holder.imageView.setImageResource(R.drawable.truck3x);
        else if (typeImage.equals("עובד"))
            holder.imageView.setImageResource(R.drawable.face3x);


    }
}
