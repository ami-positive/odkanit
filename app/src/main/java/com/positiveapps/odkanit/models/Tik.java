package com.positiveapps.odkanit.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NIVO on 30/12/2015.
 */
public class Tik {


    long id;

    @SerializedName("Name")
    String Name;
    @SerializedName("Number")
    String number;

    public Tik() {
    }

    public Tik(long id, String name, String number) {
        this.id = id;
        Name = name;
        this.number = number;
    }

    public Tik(String name, String number) {
        Name = name;
        this.number = number;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
