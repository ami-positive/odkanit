/**
 * 
 */
package com.positiveapps.odkanit.util;

import android.media.MediaPlayer;

import com.positiveapps.odkanit.main.MyApp;


/**
 * @author natiapplications
 *
 */
public class SoundUtil {
	
	public static void playSound(int soundFileId) {
		MediaPlayer mediaPlayer = MediaPlayer.create(MyApp.appContext,
				soundFileId);
		mediaPlayer.start();
	}

}
