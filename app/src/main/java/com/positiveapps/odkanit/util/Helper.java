package com.positiveapps.odkanit.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.File;



/**
 * Created by NIVO on 29/12/2015.
 */
public class Helper {

    public static final String APP_TAG = "odkanit";
    public static final String APP_FOLDER = "/odkanit";
    public static final String CAMERA_PROFILE_PATH = APP_FOLDER + "/picture.jpg";

    public static final int GALLARY_REQUEST_CODE = 1001;
    public static final int CAMERA_REQUEST_CODE = 1002;
    
    public static void createSDFolder() {
        File folder = new File(Environment.getExternalStorageDirectory() + APP_FOLDER);
        if (!folder.exists()) {
            Log.d(APP_TAG, "create folder on sd card " + APP_FOLDER);
            folder.mkdir();
        }
    }

    public static void openCamera(Fragment fragment) {
        Log.d(APP_TAG, "open camera");
        createSDFolder();
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri imageFileUri = Uri.parse("file:///sdcard/" + CAMERA_PROFILE_PATH);
        takePicture.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageFileUri);
        fragment.startActivityForResult(takePicture, CAMERA_REQUEST_CODE);
    }

    public static void openCamera(Activity activity) {
        Log.d(APP_TAG, "open camera");
        createSDFolder();
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri imageFileUri = Uri.parse("file:///sdcard/" + CAMERA_PROFILE_PATH);
        takePicture.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageFileUri);
        activity.startActivityForResult(takePicture, CAMERA_REQUEST_CODE);
    }

    public static void openGallery(Fragment fragment) {
        Log.d(APP_TAG, "open galery");
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        fragment.startActivityForResult(pickPhoto, GALLARY_REQUEST_CODE);

    }

    public static void openGallery(Activity activity) {
        Log.d(APP_TAG, "open galery");
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(pickPhoto, GALLARY_REQUEST_CODE);

    }

    public static String getProfileImageAsBase64(Bitmap selectedImage) {
        if (selectedImage != null) {
            return BitmapUtil.getImageAsStringEncodedBase64(selectedImage);
        }
        return "";
    }



    /*Uri uri = Uri.parse("geo:0,0?q=" + location.replace(" ", "+"));
				showMap(uri);
				*/
    public static void showMap(Uri geoLocation, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(intent);
        }
    }

    public static void openDialer(Context context, String tel){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" +tel));
        context.startActivity(intent);
    }

    public static void onClickEmail(String title, String text, String recipient, Activity activity){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{recipient});
        i.putExtra(Intent.EXTRA_SUBJECT, title);
        i.putExtra(Intent.EXTRA_TEXT   , text);
        try {
            activity.startActivity(Intent.createChooser(i, "שולח דואר..."));
        } catch (android.content.ActivityNotFoundException ex) {
            ToastUtil.toaster("There are no email clients installed.", false);
        }
    }


}
