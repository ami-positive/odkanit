package com.positiveapps.odkanit.network.requests;


import java.util.HashMap;

import javax.xml.datatype.XMLGregorianCalendar;

/***************************************---- Request Class ------********************/

public class NotifyWorkHours {

    private int userID;
    private int notifyCode;
    private String time;
    private int geoLong;
    private int geoLat;

    public NotifyWorkHours() {
    }

    public NotifyWorkHours(int userID,int notifyCode,String time,int geoLong,int geoLat) {
        this.userID = userID;
        this.notifyCode = notifyCode;
        this.time = time;
        this.geoLong = geoLong;
        this.geoLat = geoLat;
    }




    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("userID",userID);
        result.put("notifyCode",notifyCode);
        result.put("time",time);
        result.put("geoLong",geoLong);
        result.put("geoLat",geoLat);
        return result;
    }
}
