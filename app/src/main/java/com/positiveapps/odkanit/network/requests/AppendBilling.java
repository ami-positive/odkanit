package com.positiveapps.odkanit.network.requests;


import org.joda.time.DateTime;

import java.util.HashMap;

import javax.xml.datatype.XMLGregorianCalendar;

/***************************************---- Request Class ------********************/

public class AppendBilling {

    private int userID;
    private String tikNumber;
    private String date;
    private double hours;
    private String description;
    private double geoLong;
    private double geoLat;

    public AppendBilling() {
    }

    public AppendBilling(int userID,String tikNumber,String date,double hours,String description, double geoLong, double geoLat) {
        this.userID = userID;
        this.tikNumber = tikNumber;
        this.date = date;
        this.hours = hours;
        this.description = description;
        this.geoLong = geoLong;
        this.geoLat = geoLat;
    }





    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("userID",userID);
        result.put("tikNumber",tikNumber);
        result.put("date",date);
        result.put("hours",hours);
        result.put("description",description);
        result.put("geoLong",geoLong);
        result.put("geoLat",geoLat);
        return result;
    }
}
