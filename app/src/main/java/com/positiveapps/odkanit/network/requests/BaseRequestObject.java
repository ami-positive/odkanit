package com.positiveapps.odkanit.network.requests;

import com.positiveapps.odkanit.network.ApiInterface;
import com.positiveapps.odkanit.network.NetworkCallback;
import com.positiveapps.odkanit.network.NetworkManager;
import com.positiveapps.odkanit.network.ResponseInterface;
import com.positiveapps.odkanit.util.ToastUtil;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by natiapplications on 16/08/15.
 */
public abstract class BaseRequestObject<T/*extends BaseResponseObject*/> implements Callback<T>{


    protected ApiInterface apiInterface;


    protected NetworkCallback<T> networkCallback;

    protected abstract void execute(ApiInterface apiInterface, Callback<T> callback);
    protected abstract boolean showDefaultToastsOnError();

    public void request (ApiInterface apiInterface, final NetworkCallback<T> networkCallback){
        this.apiInterface = apiInterface;
        this.networkCallback = networkCallback;


        if (NetworkManager.isNetworkAvailable()){
            execute(apiInterface, this);
        }else{
            finishOnError("אינטרנט לא זמין");
        }


    }

    @Override
    public void success(T responseObject, Response response) {
        if (responseObject == null){
            finishOnError("שגיאה בקבלת הנתונים מהשרת");
            return;
        }

        //TODO in real mode remove casting
        if (!((ResponseInterface)responseObject).isHasError()) {


            if (networkCallback != null) {
                networkCallback.onResponse(true, null, responseObject);
            }
        } else {
            //TODO in real mode remove casting

            finishOnError(((ResponseInterface)responseObject).getErrorMessage());
        }
    }


    @Override
    public void failure(RetrofitError error) {
        finishOnError("שגיאה: " + error.getMessage());
    }



    protected  void finishOnError (String error){
        showErrorToast(error);
        if (networkCallback != null) {
            networkCallback.onResponse(false, error,null);
        }
    }


    public void showErrorToast (String errorMessage){
        if (showDefaultToastsOnError()){
            ToastUtil.toaster(errorMessage, false);
        }
    }



}
