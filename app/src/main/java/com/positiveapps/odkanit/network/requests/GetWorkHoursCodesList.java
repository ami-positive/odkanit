package com.positiveapps.odkanit.network.requests;


import java.util.HashMap;

/***************************************---- Request Class ------********************/

public class GetWorkHoursCodesList {

    private int userID;

    public GetWorkHoursCodesList() {
    }

    public GetWorkHoursCodesList(int userID) {
        this.userID = userID;
    }




    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("userID",userID);
        return result;
    }
}
