package com.positiveapps.odkanit.network;

import com.google.gson.Gson;
import com.positiveapps.odkanit.network.requests.AppendExpensePost;
import com.positiveapps.odkanit.network.response.ActionResult;
import com.positiveapps.odkanit.network.response.ClientItem;
import com.positiveapps.odkanit.network.response.LoginResult;
import com.positiveapps.odkanit.network.response.PhonebookItem;
import com.positiveapps.odkanit.network.response.ReportItem;
import com.positiveapps.odkanit.network.response.TikItem;
import com.positiveapps.odkanit.network.response.WorkHoursCodeItem;

import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.datatype.XMLGregorianCalendar;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.QueryMap;
import retrofit.mime.TypedInput;

/**
 * Created by natiapplications on 16/08/15.
 */
public interface ApiInterface {

    @GET("/Login")
    void login(@QueryMap HashMap<String,Object> body,
    	Callback<LoginResult> callback);

    @GET("/ClientsList")
    void clientsList(@QueryMap HashMap<String,Object> body,
    	Callback<ArrayList<ClientItem>> callback);

    @GET("/TiksList")
    void tiksList(@QueryMap HashMap<String,Object> body,
    	Callback<ArrayList<TikItem>> callback);

    @GET("/AppendBilling")
    void appendBilling(@QueryMap HashMap<String,Object> body,
    	Callback<ActionResult> callback);

//    @FormUrlEncoded
    //"/AppendExpensePost"
//    @GET("/AppendExpense")
//    void appendExpense(@QueryMap HashMap<String,Object> body,
//    	Callback<ActionResult> callback);

    @GET("/NotifyWorkHours")
    void notifyWorkHours(@QueryMap HashMap<String,Object> body,
    	Callback<ActionResult> callback);

    @GET("/GetWorkHoursCodesList")
    void getWorkHoursCodesList(@QueryMap HashMap<String,Object> body,
    	Callback<ArrayList<WorkHoursCodeItem>> callback);

    @GET("/GetPhoneBook")
    void getPhoneBook(@QueryMap HashMap<String,Object> body,
    	Callback<ArrayList<PhonebookItem>> callback);

    @GET("/GetOfficeID")
    void getOfficeID(@QueryMap HashMap<String,Object> body,
    	Callback<String> callback);

    @GET("/GetEntryTime")
    void getEntryTime(@QueryMap HashMap<String,Object> body,
    	Callback<String> callback);


    @GET("/GetReport")
    void getReport(@QueryMap HashMap<String,Object> body,
                   Callback<ArrayList<ReportItem>> callback);

    @POST("/AppendExpensePost")
    void appendExpensePost(@Body HashMap<String,Object> body,
                   Callback<ActionResult> callback);

//    @FormUrlEncoded
//    @POST("/AppendExpensePost")
//    void appendExpensePost(@Field("UserID") int userid ,
//                           @Field("TikNumber") String tikNumber ,
//                           @Field("Date") XMLGregorianCalendar date ,
//                           @Field("Amount") double amount ,
//                           @Field("Description") String description ,
//                           @Field("Reference") String reference ,
//                           @Field("ImageBase64") String imageBase64 ,
//                       Callback<ActionResult> callback);

//    @POST("/AppendExpensePost")
//    void appendExpensePost(@Body String json,
//                           Callback<ActionResult> callback);
//
//    @POST("/AppendExpensePost")
//    void appendExpensePost(@Body TypedInput json,
//                           Callback<ActionResult> callback);

/*

    *//***************************************---- Login function ------********************//*

    MyApp.networkManager.initRestAdapter().login(new Login(""*//*officeCode*//*,""*//*userName*//*,""*//*password*//*,""*//*deviceType*//*,""*//*deviceUDID*//*).bodyMap(), new Callback<LoginResult>() {
                @Override
                public void success(LoginResult login, Response response) {
                    ToastUtil.toaster(login.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });


    *//***************************************---- ClientsList function ------********************//*

    MyApp.networkManager.initRestAdapter().clientsList(new ClientsList(0*//*userID*//*,""*//*searchString*//*).bodyMap(), new Callback<ArrayList<ClientItem>>() {
                @Override
                public void success(ArrayList<ClientItem> clientsList, Response response) {
                    ToastUtil.toaster(clientsList.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });


    *//***************************************---- TiksList function ------********************//*

    MyApp.networkManager.initRestAdapter().tiksList(new TiksList(0*//*userID*//*,""*//*clientNumber*//*,""*//*searchString*//*).bodyMap(), new Callback<ArrayList<TikItem>>() {
                @Override
                public void success(ArrayList<TikItem> tiksList, Response response) {
                    ToastUtil.toaster(tiksList.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });


    *//***************************************---- AppendBilling function ------********************//*

    MyApp.networkManager.initRestAdapter().appendBilling(new AppendBilling(0*//*userID*//*,""*//*tikNumber0*//*date*//*,0*//*hours*//*,""*//*description*//*,0*//*geoLong*//*,0*//*geoLat*//*).bodyMap(), new Callback<ActionResult>() {
                @Override
                public void success(ActionResult appendBilling, Response response) {
                    ToastUtil.toaster(appendBilling.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });


    *//***************************************---- AppendExpense function ------********************//*

    MyApp.networkManager.initRestAdapter().appendExpense(new AppendExpense(0*//*userID*//*,""*//*tikNumber*//*,0*//*date*//*,0*//*amount*//*,""*//*description*//*,""*//*reference*//*,""*//*imageBase64*//*).bodyMap(), new Callback<ActionResult>() {
                @Override
                public void success(ActionResult appendExpense, Response response) {
                    ToastUtil.toaster(appendExpense.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });


    *//***************************************---- NotifyWorkHours function ------********************//*

    MyApp.networkManager.initRestAdapter().notifyWorkHours(new NotifyWorkHours(0*//*userID*//*,0*//*notifyCode*//*,0*//*time*//*,0*//*geoLong*//*,0*//*geoLat*//*).bodyMap(), new Callback<ActionResult>() {
                @Override
                public void success(ActionResult notifyWorkHours, Response response) {
                    ToastUtil.toaster(notifyWorkHours.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });


    *//***************************************---- GetWorkHoursCodesList function ------********************//*

    MyApp.networkManager.initRestAdapter().getWorkHoursCodesList(new GetWorkHoursCodesList(0*//*userID*//*).bodyMap(), new Callback<ArrayList<WorkHoursCodeItem>>() {
                @Override
                public void success(ArrayList<WorkHoursCodeItem> getWorkHoursCodesList, Response response) {
                    ToastUtil.toaster(getWorkHoursCodesList.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });


    *//***************************************---- GetPhoneBook function ------********************//*

    MyApp.networkManager.initRestAdapter().getPhoneBook(new GetPhoneBook(0*//*userID*//*,""*//*searchString*//*).bodyMap(), new Callback<ArrayList<PhonebookItem>>() {
                @Override
                public void success(ArrayList<PhonebookItem> getPhoneBook, Response response) {
                    ToastUtil.toaster(getPhoneBook.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });


    *//***************************************---- GetOfficeID function ------********************//*

    MyApp.networkManager.initRestAdapter().getOfficeID(new GetOfficeID(""*//*GetOfficeIDResult*//*).bodyMap(), new Callback<String>() {
                @Override
                public void success(String getOfficeID, Response response) {
                    ToastUtil.toaster(getOfficeID.toString(), true);
                    }
                @Override
                	public void failure(RetrofitError error) {
                	ToastUtil.toaster(error.toString(), true);
                    	}
            });*/


}
