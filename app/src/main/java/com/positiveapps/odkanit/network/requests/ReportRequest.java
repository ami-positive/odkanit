package com.positiveapps.odkanit.network.requests;

import java.util.HashMap;

/**
 * Created by Niv on 3/9/2016.
 */
public class ReportRequest  {
    private int userId;
    private int daysBack;

    public ReportRequest(int userId, int daysBack) {
        this.userId = userId;
        this.daysBack = daysBack;
    }

    public ReportRequest() {
    }

    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("userId",userId);
        result.put("daysBack",daysBack);

        return result;
    }
}
