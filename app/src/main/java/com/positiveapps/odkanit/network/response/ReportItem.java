package com.positiveapps.odkanit.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.odkanit.models.Report;
import com.positiveapps.odkanit.util.DateUtil;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Created by Niv on 3/9/2016.
 */
public class ReportItem implements  Comparable<ReportItem>{
//    "Amount": 1,
//            "ClientName": "שידור לבנועם",
//            "ClientNumber": "1994",
//            "Date": "2016-03-07T00:00:00.0000000",
//            "Description": "",
//            "TikName": "שידור לבנועם ",
//            "TikNumber": "1994/1",
//            "Type": "Billing"

    private double Amount;
    private String ClientName;
    private String ClientNumber;
    @SerializedName("Date")
    private String date;
    private String Description;
    private String TikName;
    private String TikNumber;
    private String Type;


    public ReportItem(double amount, String clientName, String clientNumber, String date, String description, String tikName, String tikNumber, String type) {
        Amount = amount;
        ClientName = clientName;
        ClientNumber = clientNumber;
        this.date = date;
        Description = description;
        TikName = tikName;
        TikNumber = tikNumber;
        Type = type;
    }

    @Override
    public String toString() {
        String result= "Amount="+Amount+" "+
                "ClientName="+ClientName+" "+
                "ClientNumber="+ClientNumber+" "+
                "Date="+date+" "+
                "Description="+Description+" "+
                "TikName="+TikName+" "+
                "TikNumber="+TikNumber+" "+
                "Type="+Type+" ";


        return result;

    }
    @Override
    public int compareTo(ReportItem report) {
        DateTime dt= new DateTime(getDate());
//        DateTime thisDate = new DateTime(DateUtil.getMillisOfParsingDateString(DateUtil.FORMAT_JUST_DATE, ));
        DateTime nextReportDate = new DateTime(report.getDate());

        if (getDate() == null || report.getDate() == null)
            return 0;
        return nextReportDate.compareTo(dt);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }


    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public String getClientNumber() {
        return ClientNumber;
    }

    public void setClientNumber(String clientNumber) {
        ClientNumber = clientNumber;
    }



    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTikName() {
        return TikName;
    }

    public void setTikName(String tikName) {
        TikName = tikName;
    }

    public String getTikNumber() {
        return TikNumber;
    }

    public void setTikNumber(String tikNumber) {
        TikNumber = tikNumber;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
