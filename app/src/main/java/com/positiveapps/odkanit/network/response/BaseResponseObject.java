/**
 * 
 */
package com.positiveapps.odkanit.network.response;

import com.google.gson.annotations.SerializedName;
import com.positiveapps.odkanit.network.ResponseInterface;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class BaseResponseObject implements Serializable,ResponseInterface{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@SerializedName("Success")
	private String success;
	@SerializedName("ErrorMessage")
	private String errorMessage;

	private String responseBody;


	public BaseResponseObject() {
		super();

	}




	@Override
	public boolean isHasError() {return ! success.equals("true");}

	@Override
	public String getErrorMessage() {
		return errorMessage;
	}


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}



	public void setErrorMessage(String errorMassage) {
		this.errorMessage = errorMassage;
	}

	public void setResponseBody (String body){
		this.responseBody = body;
	}

	public String getResponseBody() {
		return responseBody;
	}



}
