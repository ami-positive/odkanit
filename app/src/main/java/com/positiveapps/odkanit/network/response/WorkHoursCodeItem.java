package com.positiveapps.odkanit.network.response;
public class WorkHoursCodeItem {

    private int Code;
    private String Name;

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }
    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }


        @Override
        public String toString() {
        	return "WorkHoursCodeItem{" +
			"Code=" + Code + ";" + 
			"Name=" + Name + ";" + 
			'}';
	}
}
