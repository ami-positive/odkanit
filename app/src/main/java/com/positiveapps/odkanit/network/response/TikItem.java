package com.positiveapps.odkanit.network.response;
public class TikItem {

    private String Name;
    private String Number;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    public String getNumber() {
        return Number;
    }

    public void setNumber(String Number) {
        this.Number = Number;
    }


        @Override
        public String toString() {
        	return "TikItem{" +
			"Name=" + Name + ";" + 
			"Number=" + Number + ";" + 
			'}';
	}
}
