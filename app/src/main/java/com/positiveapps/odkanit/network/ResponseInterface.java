package com.positiveapps.odkanit.network;

/**
 * Created by NIVO on 30/12/2015.
 */
public interface ResponseInterface {

    public boolean isHasError();
    public String getErrorMessage();
}
