package com.positiveapps.odkanit.network.requests;


import java.util.HashMap;

/***************************************---- Request Class ------********************/

public class GetOfficeID {

    private String GetOfficeIDResult;

    public GetOfficeID() {
    }

    public GetOfficeID(String GetOfficeIDResult) {
        this.GetOfficeIDResult = GetOfficeIDResult;
    }




    public HashMap<String,Object> bodyMap() {
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("GetOfficeIDResult",GetOfficeIDResult);
        return result;
    }
}
