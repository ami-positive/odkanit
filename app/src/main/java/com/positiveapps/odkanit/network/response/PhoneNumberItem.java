package com.positiveapps.odkanit.network.response;
public class PhoneNumberItem {

    private String Number;
    private String Type;

    public String getNumber() {
        return Number;
    }

    public void setNumber(String Number) {
        this.Number = Number;
    }
    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }


        @Override
        public String toString() {
        	return "PhoneNumberItem{" +
			"Number=" + Number + ";" + 
			"Type=" + Type + ";" + 
			'}';
	}
}
