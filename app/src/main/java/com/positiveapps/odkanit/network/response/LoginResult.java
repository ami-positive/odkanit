package com.positiveapps.odkanit.network.response;
public class LoginResult {

    private boolean Approved;
    private String DisplayName;
    private int UserID;
    private String ErrorMessage;

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public boolean getApproved() {
        return Approved;
    }

    public void setApproved(boolean Approved) {
        this.Approved = Approved;
    }
    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String DisplayName) {
        this.DisplayName = DisplayName;
    }
    public int getUserID() {
        return UserID;
    }

    public void setUserID(int UserID) {
        this.UserID = UserID;
    }


        @Override
        public String toString() {
        	return "LoginResult{" +
			"Approved=" + Approved + ";" + 
			"DisplayName=" + DisplayName + ";" + 
			"UserID=" + UserID + ";" + 
			'}';
	}
}
